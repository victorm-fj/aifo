export default `
  type NetworkStatus {
    isConnected: Boolean
  }
  type AuthInfo {
    username: String
    isLoggedIn: Boolean
  }
  type Query {
    networkStatus: NetworkStatus
  }
  type Mutation {
    updateNetworkStatus(isConnected: Boolean): NetworkStatus
    updateAuthInfo(info: AuthInfo): AuthInfo
  }
`;
