// @flow
import React from 'react';
import { View } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import StepIndicator from 'react-native-step-indicator';
import { Button } from 'react-native-elements';

import Form from '../screens/connect/Form';
import Tutorial from '../screens/connect/Tutorial';
import PlaidLink from '../screens/connect/PlaidLink';
import InvitePartners from '../screens/connect/InvitePartners';
import { stepIndicatorStyles } from './styles';

export default createStackNavigator(
	{
		Form: { screen: Form },
		Tutorial: { screen: Tutorial },
		PlaidLink: { screen: PlaidLink },
		InvitePartners: { screen: InvitePartners },
	},
	{
		initialRouteName: 'Form',
		navigationOptions: ({ navigation, screenProps }) => {
			const {
				state: { routeName },
			} = navigation;
			let headerRightAction = () => screenProps.submitForm();
			// let headerRightAction = () => navigation.navigate('Tutorial');
			// let headerRightAction = () => navigation.navigate('InviteStack');
			let index = 0;
			if (routeName === 'PlaidLink') {
				return { header: null };
			}
			if (routeName === 'Tutorial') {
				index = 1;
				if (navigation.getParam('plaidLinked')) {
					headerRightAction = () => navigation.navigate('InvitePartners');
				} else {
					headerRightAction = () => navigation.navigate('PlaidLink');
				}
			} else if (routeName === 'InvitePartners') {
				index = 2;
				headerRightAction = () => navigation.navigate('InviteStack');
			}
			const headerLeft = (
				<Button
					title="Back"
					titleStyle={{ color: '#3b7cff' }}
					onPress={() => navigation.navigate('AuthChat', { connect: false })}
					clear
				/>
			);
			const headerTitle = (
				<View style={{ width: 100, height: 48, justifyContent: 'center' }}>
					<StepIndicator
						currentPosition={index}
						customStyles={stepIndicatorStyles}
						stepCount={3}
					/>
				</View>
			);
			const headerRight = (
				<Button
					title="Continue"
					titleStyle={{ color: '#3b7cff' }}
					onPress={headerRightAction}
					clear
				/>
			);
			return {
				headerLeft,
				headerTitle,
				headerRight,
			};
		},
	}
);
