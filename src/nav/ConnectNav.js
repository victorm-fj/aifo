// @flow
import * as React from 'react';
import { Alert } from 'react-native';
import { Formik } from 'formik';
import * as Yup from 'yup';
import moment from 'moment';
import { NavigationScreenProp } from 'react-navigation';

import ConnectStack from './ConnectStack';

const validation = {
	name: Yup.string()
		.min(3, 'Too short')
		.required('Required'),
	email: Yup.string()
		.email('Invalid email.')
		.required('Required.'),
};
const validationSchema = Yup.object().shape({
	firstName: validation.name,
	lastName: validation.name,
	dateBirth: validation.name,
	email: validation.email,
	postCode: validation.name,
	address: validation.name,
	city: validation.name,
});
const defaultDate = Date.now();

type Props = {
	navigation: NavigationScreenProp<*>,
	screenProps: NavigationScreenProp<*>,
};
class ConnectNav extends React.Component<Props> {
	static router = ConnectStack.router;
	onSubmitHandler = (values: Object) => {
		const {
			firstName,
			lastName,
			dateBirth,
			email,
			postCode,
			address,
			city,
		} = values;
		if (dateBirth === defaultDate) {
			const title = 'Missing field';
			const message = 'Please enter a valid birth date';
			const buttons = [{ text: 'Ok', onPress: () => {}, style: 'cancel' }];
			Alert.alert(title, message, buttons);
		} else {
			const title = 'Confirm information';
			const message = `Are these details correct?\n\n${firstName} ${lastName}\n${moment(Number(dateBirth)).format('MMMM D, YYYY')}\n${email}\n\n${postCode}\n${address}\n${city}`;
			const buttons = [
				{ text: 'Edit', onPress: () => {}, style: 'cancel' },
				{
					text: 'Continue',
					onPress: () => this.props.navigation.navigate('Tutorial'),
				},
			];
			Alert.alert(title, message, buttons);
		}
	};
	form: ?React.Ref<typeof React.Component>;
	submitForm = () => {
		if (this.form) {
			this.form.submitForm();
		}
	};
	render() {
		return (
			<Formik
				ref={(node) => (this.form = node)}
				onSubmit={this.onSubmitHandler}
				initialValues={{
					firstName: '',
					lastName: '',
					dateBirth: defaultDate,
					email: '',
					postCode: '',
					address: '',
					city: '',
				}}
				validationSchema={validationSchema}
				render={({
					setFieldValue,
					values,
					setFieldTouched,
					errors,
					touched,
					// handleSubmit,
				}) => (
					<ConnectStack
						navigation={this.props.navigation}
						screenProps={{
							...this.props.screenProps,
							setFieldValue,
							values,
							setFieldTouched,
							errors,
							touched,
							submitForm: this.submitForm,
						}}
					/>
				)}
			/>
		);
	}
}

export default ConnectNav;
