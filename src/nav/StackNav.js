import React from 'react';
import { TouchableOpacity, Image, View, Text, Share } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import { Button } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';

import InviteStack from './InviteStack';
import ConnectNav from './ConnectNav';
import Intro from '../screens/intro/Intro';
import AuthChatContainer from '../screens/authChat/AuthChatContainer';
import menu from '../../assets/images/menu.png';
import styles from './styles';

export default createStackNavigator(
	{
		Intro: { screen: Intro, navigationOptions: { header: null } },
		AuthChat: {
			screen: AuthChatContainer,
			navigationOptions: {
				headerLeft: (
					<TouchableOpacity onPress={() => {}} style={{ paddingLeft: 15 }}>
						<Image source={menu} style={{ width: 18 }} resizeMode="contain" />
					</TouchableOpacity>
				),
				headerTitle: (
					<View style={styles.titleContainerStyle}>
						<Text style={styles.titleTextStyle}>AIFO</Text>
					</View>
				),
				headerStyle: styles.headerContainerStyle,
			},
		},
		ConnectStack: {
			screen: ConnectNav,
			navigationOptions: { header: null, gesturesEnabled: false },
		},
		InviteStack: {
			screen: InviteStack,
			navigationOptions: ({ navigation }) => {
				const recordID = navigation.state.routes[0].params
					? navigation.state.routes[0].params.recordID
					: '';
				console.log('recordID', recordID);
				const headerLeft = (
					<Button
						title="Cancel"
						titleStyle={{ color: '#3b7cff' }}
						onPress={() => navigation.navigate('AuthChat', { connect: false })}
						clear
					/>
				);
				const title = 'Share';
				const headerRight = (
					<TouchableOpacity
						onPress={async () => {
							const referralLink = 'https://aifo.app/';
							const content = {
								title: recordID,
								message: `Hey Contact!\n\nI'm using AIFO to save without feeling it... ${referralLink}`,
							};
							const options = { subject: recordID };
							const { action, activityType } = await Share.share(
								content,
								options
							);
							console.log('share link -', action, activityType);
							if (action === 'sharedAction') {
								navigation.setParams({ sent: recordID });
							}
						}}
						style={{ paddingRight: 15 }}
					>
						<Ionicons name="ios-share-outline" size={26} color="#3b7cff" />
					</TouchableOpacity>
				);
				return {
					headerLeft,
					title,
					headerRight,
					gesturesEnabled: false,
				};
			},
		},
	},
	{ initialRouteName: 'Intro' }
);
