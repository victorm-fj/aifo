import { createStackNavigator } from 'react-navigation';

import Contacts from '../screens/invite/Contacts';

export default createStackNavigator(
	{
		Contacts: { screen: Contacts, navigationOptions: { header: null } },
	},
	{
		initialRouteName: 'Contacts',
	}
);
