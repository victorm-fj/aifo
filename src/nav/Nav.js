// @flow
import React from 'react';
import SplashScreen from 'react-native-splash-screen';
import { Auth } from 'aws-amplify';
import DeviceInfo from 'react-native-device-info';
import { graphql } from 'react-apollo';

import { AuthInfoQuery } from '../graphQL';
import StackNav from './StackNav';

type Props = { username: string, isLoggedIn: boolean };
type State = { deviceId: string, username: string, isLoggedIn: boolean };
class Nav extends React.Component<Props, State> {
	static router = StackNav.router;
	state = {
		deviceId: DeviceInfo.getUniqueID(),
		username: '',
		isLoggedIn: false,
	};
	async componentDidMount() {
		try {
			const user = await Auth.currentAuthenticatedUser();
			console.log('currentAuthenticated user', user);
			this.setState({ username: user.username, isLoggedIn: true });
		} catch (error) {
			console.log('currentAuthenticatedUser error', error);
		} finally {
			SplashScreen.hide();
		}
	}
	render() {
		const { deviceId, username, isLoggedIn } = this.state;
		return (
			<StackNav
				screenProps={{
					deviceId,
					username: username || this.props.username,
					isLoggedIn: isLoggedIn || this.props.isLoggedIn,
				}}
			/>
		);
	}
}

export default graphql(AuthInfoQuery, {
	props: ({ data: { authInfo } }) => authInfo,
})(Nav);
