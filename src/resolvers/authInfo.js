const authInfo = {
	defaults: {
		authInfo: {
			__typename: 'AuthInfo',
			username: '',
			isLoggedIn: false,
		},
	},
	resolvers: {
		Mutation: {
			updateAuthInfo: (_, { info }, { cache }) => {
				const data = { authInfo: { __typename: 'AuthInfo', ...info } };
				cache.writeData({ data });
				return null;
			},
		},
	},
};

export default authInfo;
