const networkStatus = {
	defaults: {
		networkStatus: { __typename: 'NetworkStatus', isConnected: false },
	},
	resolvers: {
		Mutation: {
			updateNetworkStatus: (_, { isConnected }, { cache }) => {
				const data = {
					netWorkStatus: { __typename: 'NetworkStatus', isConnected },
				};
				cache.writeData({ data });
				return null;
			},
		},
	},
};

export default networkStatus;
