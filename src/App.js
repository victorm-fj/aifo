// @flow
import React from 'react';
import { YellowBox } from 'react-native';
import Amplify from 'aws-amplify';
import { Rehydrated } from 'aws-appsync-react';
import { ApolloProvider } from 'react-apollo';

import awsmobile from './aws-exports';
import client from './client';
import Nav from './nav/Nav';

Amplify.configure(awsmobile);

type Props = {};
class App extends React.Component<Props> {
	async componentDidMount() {
		client.initQueryManager();
		await client.resetStore();
	}
	render() {
		return (
			<ApolloProvider client={client}>
				<Rehydrated>
					<Nav />
				</Rehydrated>
			</ApolloProvider>
		);
	}
}

export default App;

YellowBox.ignoreWarnings([
	'Warning: isMounted(...) is deprecated', // in plain JavaScript React classes.
	'Module RCTImageLoader requires', // main queue setup since it overrides `init`
	'Class RCTCxxModule was not exported.', // Did you forget to use RCT_EXPORT_MODULE()
]);
