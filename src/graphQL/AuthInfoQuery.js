import gql from 'graphql-tag';

export default gql`
	query AuthInfoQuery {
		authInfo @client {
			__typename
			username
			isLoggedIn
		}
	}
`;
