import gql from 'graphql-tag';

export default gql`
	subscription SubscribeToNewDevice($userId: ID!) {
		subscribeToNewDevice(userId: $userId) {
			__typename
			userId
			deviceId
			name
			brand
			enabled
		}
	}
`;
