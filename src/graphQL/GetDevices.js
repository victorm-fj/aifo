import gql from 'graphql-tag';

export default gql`
	query GetDevices($userId: ID!) {
		getDevices(userId: $userId) {
			__typename
			devices {
				__typename
				userId
				deviceId
				name
				brand
				enabled
			}
		}
	}
`;
