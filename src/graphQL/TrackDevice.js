import gql from 'graphql-tag';

export default gql`
	mutation TrackDevice(
		$userId: ID!
		$device: DeviceInput!
		$enabled: Boolean!
	) {
		trackDevice(userId: $userId, device: $device, enabled: $enabled) {
			__typename
			userId
			deviceId
			name
			brand
			enabled
		}
	}
`;
