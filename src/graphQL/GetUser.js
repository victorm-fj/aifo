import gql from 'graphql-tag';

export default gql`
	query GetUser($phoneNumber: String!) {
		getUser(phoneNumber: $phoneNumber) {
			__typename
			userId
			firstName
			phoneNumber
			devices {
				__typename
				userId
				deviceId
				phoneNumber
				name
				brand
				enabled
			}
		}
	}
`;
