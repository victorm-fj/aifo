import gql from 'graphql-tag';

export default gql`
	mutation RegisterUser(
		$username: String!
		$password: String!
		$firstName: String!
		$phoneNumber: String!
		$device: DeviceInput!
	) {
		registerUser(
			username: $username
			password: $password
			firstName: $firstName
			phoneNumber: $phoneNumber
			device: $device
		) {
			__typename
			userId
			firstName
			phoneNumber
		}
	}
`;
