import gql from 'graphql-tag';

export default gql`
	mutation UpdateAuthInfo($info: AuthInfo) {
		updateAuthInfo(info: $info) @client
	}
`;
