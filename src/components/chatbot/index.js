import ChatBot from './ChatBot';

export { default as Loading } from './steps/common/Loading';
export default ChatBot;
