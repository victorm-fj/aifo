// @flow
import * as React from 'react';
import {
	View,
	Dimensions,
	TextInput,
	ScrollView,
	Platform,
	// Alert,
	KeyboardAvoidingView,
	TouchableOpacity,
	Image,
	// Text,
} from 'react-native';
// import Voice from 'react-native-voice';
// import Sound from 'react-native-sound';
// import Permissions from 'react-native-permissions';
// import { Button } from 'react-native-elements';

import { CustomStep, OptionsStep, TextStep } from './steps/steps';
import schema from './schemas/schema';
import { getData, setData, removeData } from './storage';
import { guid } from './guid';
// import { getAudioUrl } from '../../awsActions';
import styles from './styles';
import defaultProps from './defaultProps';
import type { Props, State, TriggerData } from './types';
import { getSteps, isLastPosition, isFirstPosition, triggerOptions } from './utils';
import Loading from './steps/common/Loading';

const send = require('../../../assets/images/send.png');
const chat = require('../../../assets/images/chat.png');
const chatActive = require('../../../assets/images/chatActive.png');

const { height } = Dimensions.get('window');
// const requiredPermissions = ['microphone', 'speechRecognition'];
const keyboardVerticalOffset = height === 812 ? 85 : 65;

class ChatBot extends React.Component<Props, State> {
	static defaultProps = defaultProps;
	inputRef: ?React.Ref<typeof React.Component>;
	scrollView: ?React.Ref<typeof React.Component>;
	constructor(props: Object) {
		super(props);
		this.state = {
			loading: true,
			renderedSteps: [], // All steps visible in screen
			previousSteps: [], // All previous steps (even not rendered ones)
			currentStep: {},
			previousStep: {},
			steps: {}, // All pre defined steps
			editable: false,
			inputValue: '',
			inputInvalid: false,
			defaultUserSettings: {},
			// isRecording: false,
			permissions: {
				microphone: 'undetermined',
				speechRecognition: 'undetermined',
			},
			// speak: false,
			voiceMode: false,
			showPrompts: false, // Flag to show prompts when user taps on chat button
		};
		// Voice.onSpeechResults = this.onSpeechResults;
		// Voice.onSpeechEnd = this.onSpeechEnd;
		// Voice.onSpeechError = this.onSpeechError;
	}
	async componentDidMount() {
		const { cache, cacheName, resetConditions } = this.props;
		// Get steps plus settings according to the type of step
		const { defaultUserSettings, steps } = getSteps(this.props);
		// Check validity of every step id
		schema.checkInvalidIds(steps);
		// Get the first of all steps from pre defined steps passed as props
		const firstStep = this.props.steps[0];
		if (firstStep.message) {
			// Message can be either a function or a string, is function invoke it and return
			const { message } = firstStep;
			firstStep.message = typeof message === 'function' ? message() : message;
			steps[firstStep.id].message = firstStep.message;
		}
		let currentStep = {};
		let renderedSteps = [];
		let previousStep = {};
		let previousSteps = [];
		if (cache) {
			// If cache is enabled, get data from asyncstorage
			try {
				const data = await getData(cacheName, firstStep, steps);
				const checkStep = data.previousSteps.filter((step) =>
					step.id === resetConditions.checkStepId)[0];
				console.log('stored data', data);
				if (
					data.currentStep.trigger.indexOf(resetConditions.trigger) > -1
					&&
					(checkStep &&
						checkStep.value &&
						JSON.parse(checkStep.value) === resetConditions.conditionValue
					)
				) {
					console.log('reseting conversation, because of pending sign in step');
					await removeData(cacheName);
					currentStep = firstStep;
					renderedSteps = [steps[currentStep.id]];
					previousSteps = [steps[currentStep.id]];
				} else {
					currentStep = data.currentStep;
					renderedSteps = data.renderedSteps;
					previousSteps = data.previousSteps;
					previousStep = data.previousStep;
				}
			} catch (error) {
				// If data was not found, then conversation has just begun and we return
				// the first step as current, rendered and previous steps
				console.log('error trying to get data', error);
				currentStep = firstStep;
				renderedSteps = [steps[currentStep.id]];
				previousSteps = [steps[currentStep.id]];
			}
		} else {
			// If cache is not enabled init conversation from beginning
			currentStep = firstStep;
			renderedSteps = [steps[currentStep.id]];
			previousSteps = [steps[currentStep.id]];
		}
		// Set speak to true if currentStep is the firstStep
		// const speak = currentStep === firstStep;
		// Save in component state the current conversation state
		this.setState({
			defaultUserSettings,
			steps,
			currentStep,
			renderedSteps,
			previousSteps,
			previousStep,
			// speak,
			loading: false,
		});
	}
	componentDidUpdate() {
		const { currentStep: { id }, renderedSteps } = this.state;
		const { showOptions, botDelay } = this.props;
		if (showOptions[id]) {
			setTimeout(() => {
				const {
					stepCheckPoint,
					allowedAttempts,
					invalidStepId,
					secondStepCheckPointId = '',
				} = showOptions[id];
				if (triggerOptions(
					renderedSteps,
					stepCheckPoint,
					allowedAttempts,
					invalidStepId,
					secondStepCheckPointId
				)) {
					this.toggleOptions();
				}
			}, botDelay + 600);
		}
	}
	// componentDidMount() {
	// 	this.checkPermissions();
	// }
	// async checkPermissions() {
	// 	try {
	// 		const response = await Permissions.checkMultiple(requiredPermissions);
	// 		console.log('checkPermissions response', response);
	// 		this.setState({ permissions: response });
	// 	} catch (error) {
	// 		console.log('checkPermissions error', error);
	// 	}
	// }
	// askForPermission() {
	// 	const {
	// 		permissions: { microphone, speechRecognition },
	// 	} = this.state;
	// 	const buttons = [];
	// 	const cancelButton = {
	// 		text: 'No thanks',
	// 		onPress: () => console.log('Permission denied'),
	// 		style: 'cancel',
	// 	};
	// 	let requestPermissionsButton = null;
	// 	buttons.push(cancelButton);
	// 	if (
	// 		Platform.OS === 'ios' &&
	// 		(microphone === 'denied' || speechRecognition === 'denied')
	// 	) {
	// 		requestPermissionsButton = {
	// 			text: 'Open settings',
	// 			onPress: this.openSettings,
	// 		};
	// 	} else {
	// 		requestPermissionsButton = {
	// 			text: 'Ok',
	// 			onPress: () =>
	// 				requiredPermissions.forEach((permission) => {
	// 					this.requestPermission(permission);
	// 				}),
	// 		};
	// 	}
	// 	buttons.push(requestPermissionsButton);
	// 	Alert.alert(
	// 		'Instead of reading messages we can talk to each other.',
	// 		'To get started you need to enable Microphone and Speech Recognition.',
	// 		buttons
	// 	);
	// }
	// Only works for iOS
	// async openSettings() {
	// 	try {
	// 		await Permissions.openSettings();
	// 		alert('Back to app!');
	// 	} catch (error) {
	// 		console.log('error opening settings', error);
	// 	}
	// }
	// async requestPermission(permission: string) {
	// 	const response = await Permissions.request(permission);
	// 	this.setState({
	// 		permissions: { ...this.state.permissions, [permission]: response },
	// 	});
	// }
	// startRecognizing = async () => {
	// 	const { locale } = this.props;
	// 	const {
	// 		permissions: { microphone, speechRecognition },
	// 	} = this.state;
	// 	if (microphone === 'authorized' && speechRecognition === 'authorized') {
	// 		try {
	// 			await Voice.start(locale);
	// 			this.setState({ isRecording: true, voiceMode: true });
	// 		} catch (error) {
	// 			console.log('startRecognizing error', error);
	// 		}
	// 	} else {
	// 		this.askForPermission();
	// 	}
	// };
	// stopRecognizing = async () => {
	// 	const { inputValue, currentStep } = this.state;
	// 	try {
	// 		await Voice.stop();
	// 		this.setState({ isRecording: false });
	// 		if (currentStep.options) {
	// 			currentStep.options.forEach(({ value, label }) => {
	// 				if (
	// 					label
	// 						.replace(',', '')
	// 						.toLowerCase()
	// 						.indexOf(inputValue.toLowerCase()) >= 0
	// 				) {
	// 					this.setState({ speak: true });
	// 					this.triggerNextStep({ value });
	// 				}
	// 			});
	// 		} else {
	// 			this.onButtonPress();
	// 		}
	// 	} catch (error) {
	// 		console.log('stopRecognizing error', error);
	// 	}
	// };
	// onSpeechEnd = () => {
	// 	// This may onle be needed for iOS
	// 	console.log('onSpeechEnd');
	// 	this.setState({ inputValue: '' });
	// };
	// onSpeechResults = (event: Object) => {
	// 	console.log('onSpeechResults', event);
	// 	this.setState({ inputValue: event.value[0] });
	// };
	// onSpeechError = (event: Object) => {
	// 	console.log('onSpeechError error', event.error);
	// 	// Alert.alert();
	// };
	// async playMessage(message: string) {
	// 	const { locale } = this.props;
	// 	this.setState({ speak: false });
	// 	console.log('playMessage', message);
	// 	const url = await getAudioUrl(message, locale);
	// 	this.sound = new Sound(url, null, (err) => {
	// 		if (err) {
	// 			console.log('playMessage error', err);
	// 			return;
	// 		}
	// 		this.sound.play();
	// 	});
	// }
	// Handle user input in User Steps
	onButtonPress = () => {
		const {
			renderedSteps,
			previousSteps,
			inputValue,
			defaultUserSettings,
		} = this.state;
		if (inputValue === '') {
			// If user has not input anything but taps on send button just return
			return;
		}
		let { currentStep } = this.state;
		// User steps has a validator property to check input's validity
		// If present validate input using the defined function
		const isInvalid = currentStep.validator && this.checkInvalidInput();
		// COntinue if input is valid; update state and clear inputValue
		if (!isInvalid) {
			const step = {
				message: inputValue,
				value: inputValue,
				timestamps: Date.now(), // Add time to step object
			};
			currentStep = Object.assign({}, defaultUserSettings, currentStep, step);
			renderedSteps.push(currentStep);
			previousSteps.push(currentStep);
			this.setState({
				currentStep,
				renderedSteps,
				previousSteps,
				editable: false,
				inputValue: '',
				// speak: true,
			});
		}
	};
	scrollToEnd = () => {
		setTimeout(() => {
			this.scrollView.scrollToEnd();
		}, 100);
	};
	onContentSizeChange = (contentWidth: number, contentHeight: number) => {
		if (contentHeight > height - 50) {
			this.scrollView.scrollToEnd();
		}
	};
	// Returns the step's message injecting { previousValue, steps }
	// if message is defined as a function
	getStepMessage = (message: any) => {
		const { previousSteps } = this.state;
		const lastStepIndex =
			previousSteps.length > 0 ? previousSteps.length - 1 : 0;
		// Get all steps messages and values
		const steps = this.generateRenderedStepsById();
		const previousValue = previousSteps[lastStepIndex].value;
		return typeof message === 'function'
			? message({ previousValue, steps })
			: message;
	};
	// Pass { value, steps } to step's trigger prop if it's a function
	getTriggeredStep = (trigger: any, value: string) => {
		const steps = this.generateRenderedStepsById();
		return typeof trigger === 'function' ? trigger({ value, steps }) : trigger;
	};
	setContentRef = (c: ?React.Ref<typeof React.Component>): void => {
		this.scrollView = c;
	};
	setInputRef = (c: ?React.Ref<typeof React.Component>): void => {
		this.inputRef = c;
	};
	// Pass renderedSteps, steps, and values to user defined handleEnd prop
	handleEnd() {
		const { previousSteps } = this.state;
		const renderedSteps = previousSteps.map((step) => {
			const { id, message, value } = step;
			return { id, message, value };
		});
		const steps = [];
		for (let i = 0, len = previousSteps.length; i < len; i += 1) {
			const { id, message, value } = previousSteps[i];
			steps[id] = { id, message, value };
		}
		const values = previousSteps
			.filter((step) => step.value)
			.map((step) => step.value);
		if (this.props.handleEnd) {
			this.props.handleEnd({ renderedSteps, steps, values });
		}
	}
	// Critical logic to navigate through the predefined steps in the conversation flow
	triggerNextStep = async (data: TriggerData) => {
		const {
			steps, defaultUserSettings, voiceMode, showPrompts
		} = this.state;
		let {
			currentStep,
			previousStep,
			previousSteps,
			renderedSteps,
		} = this.state;
		const isEnd = currentStep.end;
		// const speak =
		// 	this.state.speak || !!previousStep.component || !!previousStep.options;
		// if (voiceMode && (speak && currentStep.message && !currentStep.user)) {
		// 	const message =
		// 		typeof currentStep.message === 'function'
		// 			? currentStep.message()
		// 			: currentStep.message;
		// 	this.playMessage(message);
		// }
		// Update indicated step value
		if (data && data.update) {
			const {
				update: { stepId, value },
			} = data;
			// Mutate only previousSteps array, not renderedSteps, so that chat history is intact
			previousSteps = previousSteps.map((step) => {
				if (step.id === stepId) {
					// We want to update the value of this step
					return { ...step, value };
				}
				return step;
			});
		}
		if (data && data.self) {
			// If self flag is passed from a Custom component, then update
			// the previousStep's value with the value passed as argument
			previousStep.value = data.value;
		}
		if (data && data.value) {
			// If self flag is not present, this means currentStep is not a CusmtoStep
			// and then value belongs to the currentStep
			currentStep.value = data.value;
		}
		if (data && data.trigger) {
			// If trigger is present this means currentStep is a CustomStep
			// get the next step to be triggered according to the 'data.trigger' value id
			currentStep.trigger = this.getTriggeredStep(data.trigger, data.value);
		}
		if (data && data.goBack) {
			// If goBack flag is present this means chat is active, propmts are being shown,
			// and we want to go back to UserStep
			currentStep.replace = data.goBack;
			// Delete options prop to skip being treated like an OptionsStep
			delete currentStep.options;
		}
		if (!showPrompts && currentStep.user && !currentStep.value) {
			// If showPropmts is false, and currentStep is a UserStep with no value
			// then just return
			return;
		}
		if (isEnd) {
			this.handleEnd();
		} else if (currentStep.options && data) {
			// Handle OptionsStep as appropriate
			const option = currentStep.options.filter((o) => o.value === data.value)[0];
			const trigger = this.getTriggeredStep(option.trigger, currentStep.value);
			delete currentStep.options;
			currentStep = Object.assign(
				{},
				currentStep,
				option,
				defaultUserSettings,
				{
					user: true,
					message: option.label,
					trigger,
					timestamps: Date.now(), // Add time to step object
				}
			);
			renderedSteps.pop();
			previousSteps.pop();
			renderedSteps.push(currentStep);
			previousSteps.push(currentStep);
			this.setState({
				currentStep,
				renderedSteps,
				previousSteps,
				showPrompts: false, // hide chat button and prompts
			});
		} else if (currentStep.trigger) {
			// If not an OptionsStep
			const isReplace = currentStep.replace && !currentStep.option;
			if (isReplace) {
				renderedSteps.pop();
			}
			const trigger = this.getTriggeredStep(
				currentStep.trigger,
				currentStep.value
			);
			// Get the next step to be triggered accroding to the 'trigger' value
			// which is in fact the id of the next step in the conversation flow
			let nextStep = Object.assign({}, steps[trigger], {
				timestamps: Date.now(),
			});
			if (nextStep.message) {
				nextStep.message = this.getStepMessage(nextStep.message);
			} else if (nextStep.update) {
				// If Update Step
				const updateStep = nextStep;
				nextStep = Object.assign({}, steps[updateStep.update], {
					timestamps: Date.now(),
				});
				if (nextStep.options) {
					for (let i = 0, len = nextStep.options.length; i < len; i += 1) {
						nextStep.options[i].trigger = updateStep.trigger;
					}
				} else {
					nextStep.trigger = updateStep.trigger;
				}
			}
			nextStep.key = guid(); // Add randorm key prop
			// Now that we got the next step we set previous step to be the current step
			// and currentStep is nextStep
			previousStep = currentStep;
			currentStep = nextStep;
			// If next step to be rendered is a User Step
			if (nextStep.user) {
				this.setState({ editable: true });
				if (!voiceMode) {
					setTimeout(() => {
						if (this.inputRef) this.inputRef.focus();
					}, 300);
				}
			} else {
				renderedSteps.push(nextStep);
				previousSteps.push(nextStep);
			}
			this.setState({
				renderedSteps,
				previousSteps,
				currentStep,
				previousStep,
			});
		}
		const {
			cache,
			cacheName,
			// handleChat,
		} = this.props;
		const toggleFlag = data && (data.goBack || data.showOptions);
		// const handleChatInvalidTriggers = Object.keys(handleChat).map((key) => handleChat[key].trigger);
		if (
			cache &&
			currentStep.id !== this.props.steps[0].id &&
			!showPrompts &&
			!toggleFlag &&
			currentStep.id !== 'customLoading' &&
			// !handleChatInvalidTriggers.includes(currentStep.id)
			!(previousStep.label && currentStep.options && !currentStep.value)
		) {
			console.log('saving to storage');
			if (currentStep.user) {
				currentStep = previousStep;
				previousStep = previousSteps.slice(-2, -1);
			} else if (currentStep.options && !currentStep.value) {
				currentStep = previousStep;
				previousStep = previousSteps.slice(-2, -1);
				previousSteps = previousSteps.slice(0, -1);
				renderedSteps = renderedSteps.slice(0, -1);
			}
			try {
				await setData(cacheName, {
					currentStep,
					previousStep,
					previousSteps,
					renderedSteps,
				});
			} catch (error) {
				console.log('error trying to set data', error);
			}
		}
	};
	// Return an object in the form { stepId: { id, message, value } }
	// taking in cosideration all previousSteps in component's state
	generateRenderedStepsById = () => {
		const { previousSteps } = this.state;
		const steps = {};
		for (let i = 0, len = previousSteps.length; i < len; i += 1) {
			const { id, message, value } = previousSteps[i];
			steps[id] = { id, message, value };
		}
		return steps;
	};
	handleKeyPress = (event: Object) => {
		if (event.nativeEvent.key === 'Enter') {
			this.onButtonPress();
		}
	};
	// Validates user input in User steps using the predfined validator function
	checkInvalidInput() {
		const { currentStep, inputValue } = this.state;
		const result = currentStep.validator(inputValue);
		const value = inputValue;
		if (typeof result !== 'boolean' || !result) {
			this.setState({
				inputValue: result.toString(),
				inputInvalid: true,
				editable: false,
			});
			setTimeout(() => {
				this.setState({
					inputValue: value,
					inputInvalid: false,
					editable: true,
				});
				this.inputRef.focus();
			}, 2000);
			return true;
		}
		return false;
	}
	onChangeText = (text: string) => {
		this.setState({ inputValue: text });
	};
	doSpeak = () => {
		this.setState({ speak: true });
	};
	stopVoiceMode = () => {
		this.setState({ voiceMode: false });
	};
	// Handle chat active button taps
	toggleOptions = () => {
		this.setState(
			(prevState) => ({ showPrompts: !prevState.showPrompts }),
			this.handlePromptActions
		);
	};
	handlePromptActions = () => {
		const { showPrompts, currentStep, previousStep } = this.state;
		if (showPrompts) {
			this.triggerNextStep({
				...this.props.handleChat[currentStep.id],
				showOptions: true,
			});
			setTimeout(() => {
				if (this.inputRef) {
					this.inputRef.blur();
				}
			}, 200);
		} else {
			this.triggerNextStep({ trigger: previousStep.id, goBack: true });
			setTimeout(() => {
				if (this.inputRef) {
					this.inputRef.focus();
				}
			}, 300);
		}
	};
	renderStep = (step: Object, index: number) => {
		const { renderedSteps, previousSteps, permissions } = this.state;
		const {
			avatarStyle,
			bubbleStyle,
			customStyle,
			customDelay,
			hideBotAvatar,
			hideUserAvatar,
			botBubbleColor,
			userBubbleColor,
			botFontColor,
			userFontColor,
		} = this.props;
		const { options, component, asMessage } = step;
		const steps = {};
		const stepIndex = renderedSteps.map((s) => s.id).indexOf(step.id);
		const previousStep = stepIndex > 0 ? renderedSteps[index - 1] : {};
		for (let i = 0, len = previousSteps.length; i < len; i += 1) {
			const ps = previousSteps[i];
			steps[ps.id] = {
				id: ps.id,
				message: ps.message,
				value: ps.value,
			};
		}
		if (component && !asMessage) {
			return (
				<CustomStep
					key={index}
					delay={customDelay}
					step={step}
					steps={steps}
					style={customStyle}
					previousStep={previousStep}
					renderedSteps={renderedSteps}
					triggerNextStep={this.triggerNextStep}
				/>
			);
		}
		if (options) {
			return (
				<OptionsStep
					key={index}
					step={step}
					triggerNextStep={this.triggerNextStep}
					bubbleStyle={bubbleStyle}
					permissions={permissions}
					speak={this.doSpeak}
					highlightedColor={userBubbleColor}
					highlightedFontColor={userFontColor}
				/>
			);
		}
		return (
			<TextStep
				key={index}
				step={step}
				steps={steps}
				previousValue={previousStep.value}
				renderedSteps={renderedSteps}
				triggerNextStep={this.triggerNextStep}
				avatarStyle={avatarStyle}
				bubbleStyle={bubbleStyle}
				hideBotAvatar={hideBotAvatar}
				hideUserAvatar={hideUserAvatar}
				isFirst={isFirstPosition(step, renderedSteps)}
				isLast={isLastPosition(step, renderedSteps)}
				botBubbleColor={botBubbleColor}
				userBubbleColor={userBubbleColor}
				botFontColor={botFontColor}
				userFontColor={userFontColor}
			/>
		);
	};
	render() {
		const {
			editable,
			inputInvalid,
			inputValue,
			renderedSteps,
			// isRecording,
			voiceMode,
			currentStep,
			// previousStep,
			showPrompts,
			loading,
		} = this.state;
		const {
			botBubbleColor,
			contentStyle,
			footerStyle,
			inputStyle,
			placeholder,
			style,
			// submitButtonStyle,
			// buttonTextStyle,
			displayChat,
			// userBubbleColor,
			// userFontColor,
			inputMetadata,
		} = this.props;
		const platformBehavior = Platform.OS === 'ios' ? 'padding' : 'height';
		const isChatActive = displayChat.includes(currentStep.id) || showPrompts;
		const renderKeyboard =
			(currentStep.user && !currentStep.label) || showPrompts;
		if (loading) {
			return (
				<View
					style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
				>
					<Loading size={16} />
				</View>
			);
		}
		return (
			<View style={[styles.chatbotContainer, style]}>
				<ScrollView
					contentContainerStyle={[styles.scrollContainer, contentStyle]}
					ref={this.setContentRef}
					onContentSizeChange={this.onContentSizeChange}
					keyboardDismissMode="on-drag"
				>
					{renderedSteps.map(this.renderStep)}
				</ScrollView>
				{renderKeyboard && (
					<KeyboardAvoidingView
						behavior={platformBehavior}
						keyboardVerticalOffset={keyboardVerticalOffset}
					>
						<View
							style={[styles.footerContainer, footerStyle]}
							disabled={!editable}
							invalid={inputInvalid}
							color={botBubbleColor}
						>
							{isChatActive && (
								<TouchableOpacity
									onPress={this.toggleOptions}
									style={[styles.sendContainerStyle, { marginRight: 15 }]}
								>
									<Image
										source={showPrompts ? chatActive : chat}
										style={styles.sendIconStyle}
										resizeMode="contain"
									/>
								</TouchableOpacity>
							)}
							{/* <Button
								title={isRecording ? 'PAUSE' : 'RECORD'}
								titleStyle={buttonTextStyle}
								containerStyle={submitButtonStyle}
								buttonStyle={{ backgroundColor: botBubbleColor }}
								onPress={
									isRecording ? this.stopRecognizing : this.startRecognizing
								}
							/>
							{voiceMode && (
								<Button
									title="STOP VOICE MODE"
									titleStyle={buttonTextStyle}
									containerStyle={[submitButtonStyle, { flex: 1 }]}
									buttonStyle={{ backgroundColor: botBubbleColor }}
									onPress={this.stopVoiceMode}
								/>
							)} */}
							{!voiceMode && (
								<TextInput
									type="textarea"
									style={[styles.textInputStyle, inputStyle]}
									placeholder={
										inputMetadata[currentStep.id]
											? inputMetadata[currentStep.id].placeholder
											: placeholder
									}
									ref={this.setInputRef}
									onKeyPress={this.handleKeyPress}
									onFocus={showPrompts ? this.toggleOptions : this.scrollToEnd}
									onBlur={this.scrollToEnd}
									onChangeText={this.onChangeText}
									value={inputValue}
									underlineColorAndroid="transparent"
									invalid={inputInvalid}
									editable={editable}
									placeholderTextColor="#8392a7"
									keyboardType={
										inputMetadata[currentStep.id]
											? inputMetadata[currentStep.id].keyboardType
											: 'default'
									}
								/>
							)}
							{!voiceMode && (
								<TouchableOpacity
									onPress={this.onButtonPress}
									style={[styles.sendContainerStyle, { marginLeft: 15 }]}
								>
									<Image
										source={send}
										style={styles.sendIconStyle}
										resizeMode="contain"
									/>
								</TouchableOpacity>
							)}
						</View>
					</KeyboardAvoidingView>
				)}
			</View>
		);
	}
}

export default ChatBot;
