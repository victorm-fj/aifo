import { StyleSheet } from 'react-native';
// import { moderateScale } from 'react-native-size-matters';

export default StyleSheet.create({
	textStepContainer: {
		alignItems: 'flex-end',
		width: '100%',
	},
	imageContainer: {
		marginTop: 6,
		marginRight: 6,
		marginBottom: 10,
		marginLeft: 6,
		padding: 2,
		backgroundColor: '#fff',
		borderTopRightRadius: 21,
		borderTopLeftRadius: 21,
		borderWidth: 1,
		borderColor: '#ddd',
	},
	imageStyle: {
		height: 40,
		width: 40,
		borderRadius: 20,
	},
	item: {
		marginTop: 5, // moderateScale(5, 2),
		flexDirection: 'row',
		minHeight: 28,
	},
	itemIn: {
		marginLeft: 15,
	},
	itemOut: {
		alignSelf: 'flex-end',
		marginRight: 15,
	},
	balloon: {
		maxWidth: 300, // moderateScale(300, 2),
		paddingHorizontal: 10, // moderateScale(10, 2),
		justifyContent: 'center',
		borderRadius: 20,
	},
	arrowContainer: {
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		zIndex: -1,
		flex: 1,
	},
	arrowLeftContainer: {
		justifyContent: 'flex-end',
		alignItems: 'flex-start',
	},

	arrowRightContainer: {
		justifyContent: 'flex-end',
		alignItems: 'flex-end',
	},

	arrowLeft: {
		left: -4, // moderateScale(-4, 0.5),
	},

	arrowRight: {
		right: -4, // moderateScale(-4, 0.5),
	},
	bubbleTextStyle: {
		fontSize: 14,
		fontFamily: 'Gotham-Book',
		lineHeight: 18,
	},
	timestampsStyle: {
		fontFamily: 'Gotham-Book',
		fontSize: 12,
		color: '#8392a7',
		marginHorizontal: 15,
		marginTop: 5,
		marginBottom: 3,
	},
});
