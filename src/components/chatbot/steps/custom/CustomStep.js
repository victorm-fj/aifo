// @flow
import React from 'react';
import { View } from 'react-native';

import Loading from '../common/Loading';
import styles from './styles';

type Props = {
	delay: number,
	step: Object,
	steps: Object,
	style: Object,
	previousStep: Object,
	renderedSteps: Array<Object>,
	triggerNextStep: Function,
};
type State = { loading: boolean };
class CustomStep extends React.Component<Props, State> {
	state = {
		loading: true,
	};
	componentDidMount() {
		const { delay, step } = this.props;
		const delayed = typeof step.delay === 'undefined' ? delay : step.delay;
		const { waitAction } = step;
		setTimeout(() => {
			this.setState({ loading: false });
			if (!waitAction) {
				this.props.triggerNextStep();
			}
		}, delayed);
	}
	renderComponent = () => {
		const {
			step,
			steps,
			previousStep,
			triggerNextStep,
			renderedSteps,
		} = this.props;
		const { component } = step;
		return React.cloneElement(component, {
			step,
			steps,
			previousStep,
			renderedSteps,
			triggerNextStep,
		});
	};
	render() {
		const { loading } = this.state;
		const { style, step } = this.props;
		return (
			<View style={[styles.customStepContainer, style]}>
				{loading ? (
					<Loading color={step.loadingColor} custom />
				) : (
					this.renderComponent()
				)}
			</View>
		);
	}
}

export default CustomStep;
