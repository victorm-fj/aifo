// @flow
import React from 'react';
import { View } from 'react-native';
import Bubbles from './Bubbles';

const Loading = (props: { size: number }) => (
	<View style={{ justifyContent: 'center', alignItems: 'center' }}>
		<Bubbles size={props.size} />
	</View>
);

Loading.defaultProps = {
	size: 4,
};
export default Loading;
