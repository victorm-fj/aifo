// @flow
import * as React from 'react';
import { TouchableOpacity, Modal, View } from 'react-native';
import { Input } from 'react-native-elements';
import moment from 'moment';

import { CustomDatePicker } from '../pickers';

const getDiffDays = (valueInMs: number) => {
	let days = 0;
	const timeDiff = Date.now() - valueInMs;
	days = Math.ceil(timeDiff / (1000 * 3600 * 24));
	return days;
};

type Props = {
	setDate: Function,
	onBlur: Function,
	name: string,
	label: string,
	value: string,
	focusNext: Function,
};
type State = {
	active: boolean,
};
class FormikDateInput extends React.Component<Props, State> {
	static defaultProps = { label: '', focusNext: () => {} };
	state = { active: false };
	setSelectedDate = (date: Date) => {
		const value = date.getTime().toString();
		if (getDiffDays(Number(value)) > 1) {
			this.props.setDate(this.props.name, value);
		}
		this.togglePicker();
		setTimeout(() => this.props.focusNext(), 450);
	};
	togglePicker = () => {
		this.setState((prevState) => ({ active: !prevState.active }));
	};
	focus() {
		this.togglePicker();
	}
	render() {
		const { active } = this.state;
		const { setDate, value, ...rest } = this.props;
		let dateValue = '';
		if (getDiffDays(Number(value)) > 1) {
			dateValue = moment(Number(value)).format('MMMM D, YYYY');
		}
		return (
			<React.Fragment>
				<Modal visible={active} animationType="fade" transparent>
					<CustomDatePicker
						setSelectedDate={this.setSelectedDate}
						mode="date"
						date={new Date(Number(this.props.value))}
					/>
				</Modal>
				<TouchableOpacity
					onPress={() => this.setState({ active: true })}
					style={{ width: '100%' }}
				>
					<View pointerEvents="none">
						<Input
							label={this.props.label}
							value={dateValue}
							editable={false}
							{...rest}
						/>
					</View>
				</TouchableOpacity>
			</React.Fragment>
		);
	}
}

export default FormikDateInput;
