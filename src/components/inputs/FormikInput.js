// @flow
import * as React from 'react';
import { Input } from 'react-native-elements';

type Props = {
	onChangeText: Function,
	onBlur: Function,
	name: string,
	label?: string,
};
class FormikInput extends React.Component<Props> {
	static defaultProps = { label: '' };
	input: ?React.Ref<typeof React.Component>;
	handleChange = (value: string) => {
		const { onChangeText, name } = this.props;
		onChangeText(name, value);
	};
	handleBlur = () => {
		const { onBlur, name } = this.props;
		onBlur(name, true);
	};
	focus() {
		this.input.focus();
	}
	render() {
		const {
			onChangeText, onBlur, label, ...rest
		} = this.props;
		return (
			<Input
				ref={(input) => (this.input = input)}
				label={label}
				onChangeText={this.handleChange}
				onBlur={this.handleBlur}
				{...rest}
			/>
		);
	}
}

export default FormikInput;
