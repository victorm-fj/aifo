import React from 'react';
import { View } from 'react-native';
import { Loading } from './chatbot';

const Loader = () => (
	<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
		<Loading size={16} />
	</View>
);

export default Loader;
