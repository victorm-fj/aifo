// @flow
import * as React from 'react';
import { SectionList } from 'react-native';

// taken from https://github.com/Ajith-Pandian/SearchableFlatlist/blob/master/index.js
type ContactType = {};
type SectionType = {
	title: string,
	data: Array<ContactType>,
};
type Props = {
	sections: Array<SectionType>,
	type: string,
	searchTerm: string,
	searchProperty: string,
	renderItem: () => React.Element<typeof React.Component>,
};
class SearchableSectionList extends React.Component<Props> {
	getFilteredResults() {
		const {
			sections, type, searchProperty, searchTerm
		} = this.props;
		const filteredSections = [...sections];
		filteredSections.forEach((section) => {
			const filteredData = section.data.filter((contact) =>
				(type && type === 'words'
					? new RegExp(`\\b${searchTerm}`, 'gi').test(contact[searchProperty])
					: new RegExp(`${searchTerm}`, 'gi').test(contact[searchProperty])));
			section.data = filteredData;
		});
		return filteredSections.filter((section) => section.data.length > 0);
	}
	render() {
		return (
			<SectionList
				{...this.props}
				renderItem={this.props.renderItem}
				sections={this.getFilteredResults()}
			/>
		);
	}
}

export default SearchableSectionList;
