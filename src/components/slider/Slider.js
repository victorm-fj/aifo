// @flow
import * as React from 'react';
import { View } from 'react-native';
import Swiper from 'react-native-swiper';

import Slide from './Slide';
import styles from './styles';

type SlideItem = { id: number, image: any, text: string };
type Props = { slides: Array<SlideItem>, updateIndex: Function };
class Slider extends React.Component<Props> {
	static defaultProps = { updateIndex: () => {} };
	swiper: ?React.Ref<typeof React.Component>;
	scrollBy(index: number) {
		if (this.swiper) {
			this.swiper.scrollBy(index);
		}
	}
	render() {
		const { slides, updateIndex, ...rest } = this.props;
		return (
			<View style={styles.sliderContainer}>
				<Swiper
					ref={(swiper) => (this.swiper = swiper)}
					showsButtons={false}
					paginationStyle={{ position: 'absolute', bottom: 0 }}
					doStyle={{ marginHorizontal: 4 }}
					dotColor="#8392a7"
					activeDotColor="#3b4859"
					onIndexChanged={updateIndex}
					{...rest}
				>
					{slides.map((slide) => <Slide key={slide.id} data={slide} />)}
				</Swiper>
			</View>
		);
	}
}

export default Slider;
