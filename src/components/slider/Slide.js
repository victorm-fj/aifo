// @flow
import React from 'react';
import { View, Text, ViewPropTypes, Image } from 'react-native';

import styles from './styles';

/**
 * Returns margin percentage according to a given index
 * @param { number } index
 * @returns { string }
 */
const getMargin = (index: number): string => {
	if (index === 0) return '30%';
	else if (index === 1) return '10%';
	return '5%';
};

type Data = {
	id: number,
	image: number,
	text: string,
	unique?: boolean,
	big?: boolean,
};
type Props = { style: ViewPropTypes.style, data: Data };
const Slide = ({
	style, data: {
		id, image, text, unique, big
	}
}: Props) => (
	<View style={[styles.slideContainer, style]}>
		<View style={styles.slideImageContainer}>
			<Image
				source={image}
				style={[styles.slideImage, { marginTop: getMargin(id) }]}
				resizeMode="contain"
			/>
		</View>
		<View
			style={[
				styles.slideTextContainer,
				{
					justifyContent: unique ? 'flex-end' : 'flex-start',
				},
			]}
		>
			<Text style={[styles.slideText, { width: big ? 280 : 220 }]}>{text}</Text>
		</View>
	</View>
);
Slide.defaultProps = {
	style: {},
};

export default Slide;
