import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	sliderContainer: {
		flex: 1,
		alignItems: 'center',
		paddingVertical: 20,
		backgroundColor: '#f9fafc',
		width: '100%',
	},
	slideContainer: {
		flex: 1,
		width: '100%',
	},
	slideImageContainer: {
		flex: 1,
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center',
	},
	slideImage: {
		width: '75%',
	},
	slideTextContainer: {
		flex: 0.4,
		alignItems: 'center',
	},
	slideText: {
		fontFamily: 'Gotham-Book',
		fontSize: 18,
		lineHeight: 24,
		width: 220,
		height: 60,
		textAlign: 'center',
	},
});
