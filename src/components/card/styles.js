import { StyleSheet, Platform } from 'react-native';

export default StyleSheet.create({
	cardContainer: {
		backgroundColor: '#f9fafc',
		borderRadius: 8,
		...Platform.select({
			ios: {
				shadowColor: 'rgba(59,72,89,0.35)',
				shadowOpacity: 1,
				shadowRadius: 2,
				shadowOffset: {
					height: 1,
				},
			},
			android: {
				elevation: 6,
			},
		}),
	},
	headerContainer: {
		padding: 16,
		borderBottomColor: '#8392a7',
		flexDirection: 'row',
		alignItems: 'center',
	},
	headerTitleContainer: {
		flex: 1,
	},
	headerTitleText: {
		fontSize: 18,
		lineHeight: 18,
		fontFamily: 'Gotham-Book',
	},
	headerButtonTitleText: {
		fontSize: 16,
		fontFamily: 'Gotham-Book',
		color: '#3b7cff',
		padding: 0,
		paddingLeft: 5,
	},
	footerContainer: {
		padding: 16,
		borderTopColor: '#8392a7',
		flexDirection: 'row',
		alignItems: 'center',
	},
	rowContainer: {
		flexDirection: 'row',
		alignItems: 'center',
	},
	footerTitleText: {
		fontSize: 16,
		fontFamily: 'Gotham-Book',
	},
	footerSubtitleText: {
		fontSize: 14,
		fontFamily: 'Gotham-Book',
		color: '#8392a7',
	},
	contentContainer: {
		padding: 16,
		borderBottomColor: '#8392a7',
		borderRightColor: '#8392a7',
	},
});
