// @flow
import * as React from 'react';
import { View, ViewPropTypes } from 'react-native';

import Header from './Header';
import Content from './Content';
import Footer from './Footer';
import styles from './styles';

type Props = {
	style: ViewPropTypes.style,
	children: React.Node,
};
const Card = (props: Props) => (
	<View style={[styles.cardContainer, props.style]}>{props.children}</View>
);

Card.defaultProps = { style: {} };
Card.Header = Header;
Card.Content = Content;
Card.Footer = Footer;

export default Card;
