// @flow
import * as React from 'react';
import { View, ViewPropTypes, Text } from 'react-native';
import { Button, Icon } from 'react-native-elements';

import styles from './styles';

type Props = {
	divider: boolean,
	style: ViewPropTypes.style,
	children: React.Node,
	title: string,
	buttonTitle: string,
	icon: boolean,
};
const Header = (props: Props) => {
	const {
		style, divider, children, title, buttonTitle, icon
	} = props;
	return (
		<View
			style={[
				styles.headerContainer,
				{ borderBottomWidth: divider ? 1 : 0 },
				style,
			]}
		>
			{title && (
				<View style={styles.headerTitleContainer}>
					<Text style={styles.headerTitleText}>{title}</Text>
				</View>
			)}
			{buttonTitle && (
				<Button
					title={buttonTitle}
					titleStyle={styles.headerButtonTitleText}
					icon={
						icon ? (
							<Icon type="ionicon" name="md-add" color="#3b7cff" size={18} />
						) : null
					}
					clear
				/>
			)}
			{children}
		</View>
	);
};
Header.defaultProps = {
	divider: false,
	title: '',
	buttonTitle: '',
	icon: false,
	style: {},
	children: null,
};

export default Header;
