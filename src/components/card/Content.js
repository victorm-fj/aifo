// @flow
import * as React from 'react';
import { View, ViewPropTypes } from 'react-native';

import styles from './styles';

type Props = {
	divider: boolean,
	style: ViewPropTypes.style,
	children: React.Node,
	separator: boolean,
};
const Content = (props: Props) => (
	<View
		style={[
			styles.contentContainer,
			{
				borderBottomWidth: props.divider ? 1 : 0,
				borderRightWidth: props.separator ? 1 : 0,
			},
			props.style,
		]}
	>
		{props.children}
	</View>
);
Content.defaultProps = { divider: false, separator: false, style: {} };

export default Content;
