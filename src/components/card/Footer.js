// @flow
import * as React from 'react';
import { View, ViewPropTypes, Text } from 'react-native';
import { Icon } from 'react-native-elements';

import styles from './styles';

type Props = {
	divider: boolean,
	style: ViewPropTypes.style,
	children: React.Node,
	total: {
		totalQuantity: number,
		measure: string,
		remainingDays: number,
		recommendedQuantity: number,
	},
};
const Footer = (props: Props) => {
	const {
		style, divider, total, children
	} = props;
	return (
		<View
			style={[
				styles.footerContainer,
				{ borderTopWidth: divider ? 1 : 0 },
				style,
			]}
		>
			{props.total && (
				<View style={styles.rowContainer}>
					<Text style={styles.footerTitleText}>
						{`${total.totalQuantity} ${total.measure}`}
					</Text>
					<Icon
						type="ionicon"
						name="ios-information-circle-outline"
						containerStyle={{ marginHorizontal: 15 }}
						color="#8392a7"
						size={24}
					/>
					{total.remainingDays && (
						<Text style={styles.footerSubtitleText}>
							Remaining food for{' '}
							{`${total.remainingDays}-${total.remainingDays + 1}`} days
						</Text>
					)}
					{total.recommendedQuantity && (
						<Text style={styles.footerSubtitleText}>
							Recommended{' '}
							{`${total.recommendedQuantity}-${total.recommendedQuantity +
								0.5}`}{' '}
							liters
						</Text>
					)}
				</View>
			)}
			{children}
		</View>
	);
};
Footer.defaultProps = {
	divider: false,
	total: null,
};

export default Footer;
