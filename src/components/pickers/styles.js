import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	container: {
		flex: 1,
		position: 'absolute',
		top: 0,
		left: 0,
		bottom: 0,
		right: 0,
		zIndex: 1000,
	},
	overlay: {
		flex: 1.7,
		backgroundColor: 'rgba(0,0,0,0.3)',
	},
	pickerContainer: {
		flex: 1.3,
		backgroundColor: '#f9fafc',
	},
	buttonContainer: {
		padding: 20,
		borderTopWidth: 1,
		borderColor: '#3b4859',
	},
	buttonTitle: {
		color: '#3b7cff',
	},
});
