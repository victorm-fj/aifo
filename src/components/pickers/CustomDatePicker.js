// @flow
import React from 'react';
import { View, DatePickerIOS } from 'react-native';
import { Button } from 'react-native-elements';

import styles from './styles';

type Props = {
	setSelectedDate: (selectedDate: Date) => void,
	mode: 'date' | 'time' | 'datetime',
	date: Date,
};
type State = {
	selectedDate: Date,
};
class CustomDatePicker extends React.Component<Props, State> {
	static defaultProps = { mode: 'datetime' };
	state = { selectedDate: this.props.date || new Date() };
	onDateChangeHandler = (selectedDate: Date) => {
		this.setState({ selectedDate });
	};
	onPressDone = () => {
		this.props.setSelectedDate(this.state.selectedDate);
	};
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.overlay} />
				<View style={styles.pickerContainer}>
					<DatePickerIOS
						date={this.state.selectedDate}
						onDateChange={this.onDateChangeHandler}
						mode={this.props.mode}
					/>
					<Button
						title="Done"
						clear
						conatinerStyle={styles.buttonContainer}
						titleStyle={styles.buttonTitle}
						onPress={this.onPressDone}
					/>
				</View>
			</View>
		);
	}
}

export default CustomDatePicker;
