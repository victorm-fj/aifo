// @flow
import * as React from 'react';
import { View } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import WkWebView from 'react-native-wkwebview-reborn';
import Loader from '../../components/Loader';

const publicKey = 'e8d27754a3e4508f88db1f41144574';
const env = 'sandbox';
const product = 'transactions,auth';
const clientName = 'AIFO';
const plaidLinkURL = `https://cdn.plaid.com/link/v2/stable/link.html??isWebview=true&key=${publicKey}&apiVersion=v2&env=${env}&product=${product}&clientName=${clientName}&isMobile=true`;
// const patchPostMessageFunction = () => {
// 	const originalPostMessage = window.postMessage;
// 	const patchedPostMessage = function (message, targetOrigin, transfer) {
// 		originalPostMessage(message, targetOrigin, transfer);
// 	};

// 	patchedPostMessage.toString = () =>
// 		String(Object.hasOwnProperty).replace('hasOwnProperty', 'postMessage');
// 	window.postMessage = patchedPostMessage;
// };

type Props = { navigation: NavigationScreenProp<*> };
class PlaidLink extends React.Component<Props> {
	onContinue = () => {
		this.props.navigation.navigate('Tutorial', { plaidLinked: true });
	};
	onLoadStart = (props: Object) => {
		console.log('onLoadStart', props);
	};
	onLoad = (props: Object) => {
		console.log('onLoad', props);
	};
	onLoadEnd = (props: Object) => {
		console.log('onLoadEnd', props);
	};
	onMessage = (e: Object) => {
		const data = JSON.parse(e.nativeEvent.data);
		console.log('plaid data obj', data);
		const status = data.action
			.substr(data.action.lastIndexOf(':') + 1)
			.toUpperCase();
		switch (status) {
		case 'EXIT':
			this.goBack();
			break;
		case 'CONNECTED':
			this.onContinue();
			break;
		default:
			break;
		}
	};
	goBack = () => {
		this.props.navigation.goBack();
		// this.props.navigation.navigate('AuthChat', { connect: false });
	};
	render() {
		// const patchPostMessageJsCode = `(${String(patchPostMessageFunction)})();`;
		return (
			<View style={{ flex: 1, paddingTop: 20 }}>
				<WkWebView
					contentInsetAdjustmentBehavior="always"
					source={{ uri: plaidLinkURL }}
					onMessage={(e) => this.onMessage(e)}
					onLoad={this.onLoad}
					onLoadStart={this.onLoadStart}
					onLoadEnd={this.onLoadEnd}
					// javaScriptEnabled
					// injectedJavaScript={patchPostMessageJsCode}
					bounces={false}
					scrollEnabled={false}
					startInLoadingState
					renderLoading={() => <Loader />}
				/>
			</View>
		);
	}
}

export default PlaidLink;
