// @flow
import React from 'react';
import { View, Text } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { Button } from 'react-native-elements';

import Slider from '../../components/slider/Slider';
import commonStyles from '../commonStyles';
import slide1 from '../../../assets/images/slide1.png';
import styles from '../intro/styles';

const successSlide = [
	{
		id: 0,
		image: slide1,
		text: 'Got partners? Invite them now to join for a better...',
		unique: true,
		big: true,
	},
];

type Props = { navigation: NavigationScreenProp<*> };
const InvitePartners = (props: Props) => (
	<View
		style={{
			height: '100%',
			alignItems: 'center',
			paddingVertical: 20,
			backgroundColor: '#f9fafc',
		}}
	>
		<Slider slides={successSlide} />
		<View style={styles.actionsContainer}>
			<View
				style={{
					justifyContent: 'center',
					alignItems: 'center',
					marginBottom: 20,
				}}
			>
				<Text style={commonStyles.footnoteTextStyle}>Your code</Text>
				<Text style={[commonStyles.titleStyle, { marginTop: 0 }]}>H8XNUT</Text>
				<Text
					style={[commonStyles.footnoteTextStyle, commonStyles.textHighlighted]}
				>
					(tap to copy)
				</Text>
			</View>
			<Button
				title="Invite your partners"
				onPress={() => props.navigation.navigate('InviteStack')}
				titleStyle={styles.buttonTitleStyle}
				buttonStyle={styles.buttonStyle}
				containerStyle={[
					styles.buttonContainerStyle,
					{ paddingHorizontal: 15 },
				]}
			/>
		</View>
	</View>
);

export default InvitePartners;
