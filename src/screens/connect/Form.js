// @flow
import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { CheckBox } from 'react-native-elements';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { FormikInput, FormikDateInput } from '../../components/inputs';
import commonStyles from '../commonStyles';

type Props = {
	navigation: NavigationScreenProp<*>,
	screenProps: NavigationScreenProp<*>,
};
type State = { checked: boolean };
class Form extends React.Component<Props, State> {
	state = { checked: false };
	firstNameInput: ?React.Ref<typeof React.Component>;
	lastNameInput: ?React.Ref<typeof React.Component>;
	dateBirthInput: ?React.Ref<typeof React.Component>;
	emailInput: ?React.Ref<typeof React.Component>;
	postCodeInput: ?React.Ref<typeof React.Component>;
	addressInput: ?React.Ref<typeof React.Component>;
	cityInput: ?React.Ref<typeof React.Component>;
	componentDidMount() {
		if (this.firstNameInput) {
			this.firstNameInput.focus();
		}
	}
	render() {
		const {
			screenProps: {
				setFieldValue, values, setFieldTouched, errors, touched
			},
		} = this.props;
		return (
			<View style={{ flex: 1, backgroundColor: '#f3f5f8' }}>
				<KeyboardAwareScrollView
					contentContainerStyle={commonStyles.scrollContainer}
					extraScrollHeight={20}
					automaticallyAdjustContentInsets
				>
					<Text style={commonStyles.titleStyle}>
						Create an AIFO savings account
					</Text>
					<View style={commonStyles.formContainer}>
						<FormikInput
							ref={(node) => (this.firstNameInput = node)}
							onSubmitEditing={() => {
								this.lastNameInput.focus();
							}}
							blurOnSubmit={false}
							inputStyle={commonStyles.inputStyle}
							inputContainerStyle={commonStyles.inputContainerStyle}
							placeholderTextColor="#8392a7"
							placeholder="First name"
							containerStyle={commonStyles.inputContainer}
							name="firstName"
							onChangeText={setFieldValue}
							value={values.firstName}
							onBlur={setFieldTouched}
							errorMessage={
								errors.firstName && touched.firstName ? errors.firstName : ''
							}
							returnKeyType="next"
						/>
						<FormikInput
							ref={(node) => (this.lastNameInput = node)}
							onSubmitEditing={() => {
								this.dateBirthInput.focus();
							}}
							inputStyle={commonStyles.inputStyle}
							inputContainerStyle={commonStyles.inputContainerStyle}
							placeholderTextColor="#8392a7"
							placeholder="Last name"
							containerStyle={commonStyles.inputContainer}
							name="lastName"
							onChangeText={setFieldValue}
							value={values.lastName}
							onBlur={setFieldTouched}
							errorMessage={
								errors.lastName && touched.lastName ? errors.lastName : ''
							}
							returnKeyType="next"
						/>
						<FormikDateInput
							ref={(node) => (this.dateBirthInput = node)}
							inputStyle={commonStyles.inputStyle}
							inputContainerStyle={commonStyles.inputContainerStyle}
							placeholderTextColor="#8392a7"
							placeholder="Date of birth"
							containerStyle={commonStyles.inputContainer}
							name="dateBirth"
							value={values.dateBirth}
							setDate={setFieldValue}
							focusNext={() => {
								this.emailInput.focus();
							}}
						/>
						<FormikInput
							ref={(node) => (this.emailInput = node)}
							onSubmitEditing={() => {
								this.postCodeInput.focus();
							}}
							blurOnSubmit={false}
							inputStyle={commonStyles.inputStyle}
							inputContainerStyle={commonStyles.inputContainerStyle}
							placeholderTextColor="#8392a7"
							placeholder="Email"
							containerStyle={commonStyles.inputContainer}
							name="email"
							onChangeText={setFieldValue}
							value={values.email}
							onBlur={setFieldTouched}
							errorMessage={errors.email && touched.email ? errors.email : ''}
							keyboardType="email-address"
							returnKeyType="next"
							autoCapitalize="none"
						/>
						<Text style={commonStyles.footnoteTextStyle}>
							Please use your full name so that we can verify your identity.
						</Text>
						<CheckBox
							title="Receive important emails from us about new product features & AIFO updates"
							checked={this.state.checked}
							textStyle={commonStyles.checkboxTextStyle}
							containerStyle={commonStyles.checkboxContainer}
							onPress={() =>
								this.setState((prevState) => ({
									checked: !prevState.checked,
								}))
							}
							checkedColor="#3b7cff"
							checkedIcon="ios-checkbox-outline"
							uncheckedIcon="ios-checkbox"
							iconType="ionicon"
						/>
						<FormikInput
							ref={(node) => (this.postCodeInput = node)}
							onSubmitEditing={() => {
								this.addressInput.focus();
							}}
							blurOnSubmit={false}
							inputStyle={commonStyles.inputStyle}
							inputContainerStyle={commonStyles.inputContainerStyle}
							placeholderTextColor="#8392a7"
							placeholder="Post code"
							containerStyle={commonStyles.inputContainer}
							name="postCode"
							onChangeText={setFieldValue}
							value={values.postCode}
							onBlur={setFieldTouched}
							errorMessage={
								errors.postCode && touched.postCode ? errors.postCode : ''
							}
							returnKeyType="next"
						/>
						<FormikInput
							ref={(node) => (this.addressInput = node)}
							onSubmitEditing={() => {
								this.cityInput.focus();
							}}
							blurOnSubmit={false}
							inputStyle={commonStyles.inputStyle}
							inputContainerStyle={commonStyles.inputContainerStyle}
							placeholderTextColor="#8392a7"
							placeholder="Address"
							containerStyle={commonStyles.inputContainer}
							name="address"
							onChangeText={setFieldValue}
							value={values.address}
							onBlur={setFieldTouched}
							errorMessage={
								errors.address && touched.address ? errors.address : ''
							}
							returnKeyType="next"
						/>
						<FormikInput
							ref={(node) => (this.cityInput = node)}
							inputStyle={commonStyles.inputStyle}
							inputContainerStyle={commonStyles.inputContainerStyle}
							placeholderTextColor="#8392a7"
							placeholder="City"
							containerStyle={commonStyles.inputContainer}
							name="city"
							onChangeText={setFieldValue}
							value={values.city}
							onBlur={setFieldTouched}
							errorMessage={errors.city && touched.city ? errors.city : ''}
							returnKeyType="done"
						/>
						<Text style={commonStyles.footnoteTextStyle}>
							This needs to be the address registered with your bank - i.e. your
							billing address.
						</Text>
					</View>
				</KeyboardAwareScrollView>
			</View>
		);
	}
}

export default Form;
