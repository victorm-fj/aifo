// @flow
import * as React from 'react';
import { View } from 'react-native';
import { Button } from 'react-native-elements';
import { NavigationScreenProp } from 'react-navigation';

import Slider from '../../components/slider/Slider';
import slide1 from '../../../assets/images/slide1.png';
import slide2 from '../../../assets/images/slide2.png';
import slide3 from '../../../assets/images/slide3.png';
import styles from '../intro/styles';

const slides = [
	{ id: 0, image: slide1, text: 'Save without feeling it copy line two' },
	{ id: 1, image: slide2, text: 'Save without feeling it copy line two' },
	{ id: 2, image: slide3, text: 'Save without feeling it copy line two' },
];
const successSlide = [
	{
		id: 3,
		image: slide1,
		text: 'Do you have other personal or business banking accounts?',
	},
];

type Props = { navigation: NavigationScreenProp<*> };
type State = { activeSlide: number };
class Tutorial extends React.Component<Props, State> {
	state = { activeSlide: 0 };
	onPressHandler = () => {
		const { activeSlide } = this.state;
		if (this.slider && activeSlide < 2) {
			this.slider.scrollBy(1);
		} else {
			this.props.navigation.navigate('PlaidLink');
		}
	};
	slider: ?React.Ref<typeof React.Component>;
	updateActiveSlide = (index: number) => {
		this.setState({ activeSlide: index });
	};
	render() {
		const { navigation } = this.props;
		if (navigation.getParam('plaidLinked')) {
			return (
				<View
					style={{
						height: '100%',
						alignItems: 'center',
						paddingVertical: 20,
						backgroundColor: '#f9fafc',
					}}
				>
					<Slider slides={successSlide} />
					<View style={styles.actionsContainer}>
						<View
							style={{ flexDirection: 'row', justifyContent: 'space-around' }}
						>
							<Button
								title="Yes, I do"
								onPress={() => navigation.navigate('InvitePartners')}
								titleStyle={styles.buttonTitleStyle}
								buttonStyle={[styles.buttonStyle, { paddingHorizontal: 10 }]}
								containerStyle={[
									styles.buttonContainerStyle,
									{ marginBottom: 40, marginHorizontal: 5 },
								]}
							/>
							<Button
								title="Nope, I don't"
								onPress={() => navigation.navigate('InvitePartners')}
								titleStyle={[styles.buttonTitleStyle, { color: '#3b7cff' }]}
								buttonStyle={[
									styles.buttonStyle,
									{
										backgroundColor: '#ffffff',
										borderColor: '#3b7cff',
										borderWidth: 1,
										paddingHorizontal: 10,
									},
								]}
								containerStyle={[
									styles.buttonContainerStyle,
									{ marginBottom: 40, marginHorizontal: 5 },
								]}
								clear
							/>
						</View>
					</View>
				</View>
			);
		}
		return (
			<View style={styles.introContainer}>
				<Slider
					ref={(slider) => (this.slider = slider)}
					slides={slides}
					loop={false}
					updateIndex={this.updateActiveSlide}
				/>
				<View style={styles.actionsContainer}>
					<Button
						title={this.state.activeSlide === 2 ? 'Continue' : 'Next'}
						onPress={this.onPressHandler}
						titleStyle={styles.buttonTitleStyle}
						buttonStyle={styles.buttonStyle}
						containerStyle={[styles.buttonContainerStyle, { minWidth: 230 }]}
					/>
				</View>
			</View>
		);
	}
}

export default Tutorial;
