// @flow
import React from 'react';
import { Subscribe } from 'unstated';
import { graphql } from 'react-apollo';

import { UpdateAuthInfo } from '../../../graphQL';
import { AuthContainer } from '../AuthContainer';

import ConfirmCode from './ConfirmCode';

const ConfirmUserContainer = (props: Object) => (
	<Subscribe to={[AuthContainer]}>
		{(authStore) => (
			<ConfirmCode cognitoUser={authStore.state.cognitoUser} {...props} />
		)}
	</Subscribe>
);

export default graphql(UpdateAuthInfo, {
	props: ({ mutate }) => ({
		updateAuthInfo: (info) => mutate({ variables: { info } }),
	}),
})(ConfirmUserContainer);
