// @flow
import React from 'react';
import { Auth } from 'aws-amplify';

import { Loading } from '../../../components/chatbot';

type Props = {
	triggerNextStep: Function,
	steps: Object,
	id: string,
	cognitoUser: Object,
	updateAuthInfo: Function,
};
class ConfirmCode extends React.Component<Props> {
	async componentDidMount() {
		const {
			triggerNextStep,
			steps,
			id,
			cognitoUser,
			updateAuthInfo,
		} = this.props;
		// Get code from corresponding step value
		const code = steps[`confirmCode${id}`].value.trim();
		let authUserValue = steps.authUser.value;
		const isCodeValid = code.length === 6 && !Number.isNaN(Number(code));
		if (isCodeValid && authUserValue) {
			authUserValue = JSON.parse(authUserValue);
			if (authUserValue === 'registeredUser' && cognitoUser) {
				try {
					await Auth.sendCustomChallengeAnswer(cognitoUser, code);
					// Update local cache
					updateAuthInfo({ username: cognitoUser.username, isLoggedIn: true });
					triggerNextStep({ value: undefined, trigger: '5' });
					setTimeout(() => triggerNextStep({ trigger: 'loginSuccess' }), 0);
				} catch (error) {
					console.log('sendCustomChallengeAnswer error', error);
					triggerNextStep({
						value: undefined,
						trigger: 'invalidCode1',
						self: true,
					});
					setTimeout(() => triggerNextStep({ trigger: 'confirmCode2' }), 0);
				}
			} else if (typeof authUserValue === 'object') {
				// confirmSignUp and authenticate newly registered user
				const { username, password } = authUserValue;
				try {
					await Auth.confirmSignUp(username, code);
					await Auth.signIn(username, password);
					updateAuthInfo({ username, isLoggedIn: true });
					triggerNextStep({ value: undefined, trigger: '5' });
					setTimeout(() => triggerNextStep({ trigger: 'loginSuccess' }), 0);
				} catch (error) {
					console.log('Confirm user account error', error);
					triggerNextStep({
						value: undefined,
						trigger: 'invalidCode1',
						self: true,
					});
					setTimeout(() => triggerNextStep({ trigger: 'confirmCode2' }), 0);
				}
			}
		} else {
			triggerNextStep({
				value: undefined,
				trigger: 'invalidCode1',
				self: true,
			});
			setTimeout(() => triggerNextStep({ trigger: 'confirmCode2' }), 0);
		}
	}
	render() {
		return <Loading />;
	}
}

export default ConfirmCode;
