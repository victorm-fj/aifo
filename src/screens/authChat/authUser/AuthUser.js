// @flow
import React from 'react';
import DeviceInfo from 'react-native-device-info';
import { Auth } from 'aws-amplify';
import uuidV4 from 'uuid/v4';

import { Loading } from '../../../components/chatbot';

type Props = {
	triggerNextStep: Function,
	steps: Object,
	onRegisterUser: Function,
	addCognitoUser: Function,
	onTrackDevice: Function,
	loading: boolean,
	error: any,
	user: Object,
};
class AuthUser extends React.Component<Props> {
	async componentDidUpdate() {
		if (!this.props.loading) {
			const {
				triggerNextStep,
				steps: { name, phoneNumber1, phoneNumber2 },
				onRegisterUser,
				addCognitoUser,
				onTrackDevice,
				user,
			} = this.props;
			const deviceId = DeviceInfo.getUniqueID();
			const deviceName = DeviceInfo.getDeviceName();
			const deviceBrand = DeviceInfo.getBrand();
			// If user has not chosen 'Get new code' option, then getNewCode is undefined
			const getNewCode = this.props.steps.invalidCodeOptions;
			try {
				if (user && user.userId) {
					// User is already registered
					console.log('user', user);
					const {
						userId, firstName, devices, phoneNumber
					} = user;
					let isDeviceTracked = false;
					for (let index = 0; index < devices.length; index += 1) {
						const device = devices[index];
						if (device.deviceId === deviceId && device.enabled) {
							isDeviceTracked = true;
						}
					}
					if (isDeviceTracked) {
						// Device is a tracked device
						if (getNewCode && getNewCode.value) {
							// User has chosen to get a new confirmation signup code
							await Auth.resendSignUp(userId);
							// Reset invalidCodeOptions value to be undefined so
							// that next time user can confirm account
							triggerNextStep({
								trigger: '4',
								update: { stepId: 'invalidCodeOptions', value: undefined },
							});
							setTimeout(() => triggerNextStep({ trigger: 'confirmCode1' }), 0);
						} else {
							console.log('init auth');
							const cognitoUser = await Auth.signIn(userId);
							console.log('cognitoUser', cognitoUser);
							// Save cognitoUser instance to AuthContainer state
							addCognitoUser(cognitoUser);
							// Just in case update 'name' step with the value retrieved in DB
							const update = { stepId: 'name', value: firstName };
							triggerNextStep({
								trigger: '4',
								value: JSON.stringify('registeredUser'),
								update,
							});
							setTimeout(() => triggerNextStep({ trigger: 'confirmCode1' }), 0);
						}
					} else {
						// device is not already tracked, then save device in DB
						console.log('save new device');
						onTrackDevice({
							userId,
							device: {
								deviceId,
								phoneNumber,
								name: deviceName,
								brand: deviceBrand,
							},
							enabled: false,
						});
						// trigger step explaining to try again
						triggerNextStep({
							trigger: 'trySignInAgain',
							value: 'invalidDevice',
						});
						setTimeout(() => triggerNextStep({ trigger: 'phoneNumber1' }), 0);
					}
				} else {
					// User is not registered yet, register him
					console.log('user is not registered');
					const username = uuidV4();
					const password = `AIFO-${uuidV4().slice(0, 13)}`;
					const phoneNumber = phoneNumber1.value
						? phoneNumber1.value
						: phoneNumber2.value;
					onRegisterUser({
						username,
						password,
						firstName: name.value,
						phoneNumber,
						device: { deviceId, name: deviceName, brand: deviceBrand },
					});
					triggerNextStep({
						trigger: '4',
						value: JSON.stringify({ username, password }),
					});
					setTimeout(() => triggerNextStep({ trigger: 'confirmCode1' }), 0);
				}
			} catch (error) {
				console.log('AuthUser error', error);
			}
		}
	}
	render() {
		return <Loading />;
	}
}

export default AuthUser;
