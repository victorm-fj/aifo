// @flow
import React from 'react';
import { Subscribe } from 'unstated';
import { compose, graphql } from 'react-apollo';

import { GetUser, RegisterUser, TrackDevice } from '../../../graphQL';
import { AuthContainer } from '../AuthContainer';
import AuthUser from './AuthUser';

const AuthUserContainer = (props: Object) => (
	<Subscribe to={[AuthContainer]}>
		{(authStore) => (
			<AuthUser
				addCognitoUser={authStore.addCognitoUser}
				// devices={authStore.state.devices}
				{...props}
			/>
		)}
	</Subscribe>
);

export default compose(
	graphql(GetUser, {
		options: (props) => ({
			fetchPolicy: 'network-only',
			variables: {
				phoneNumber: props.steps.phoneNumber1.value
					? props.steps.phoneNumber1.value
					: props.steps.phoneNumber2.value,
			},
		}),
		props: (props) => ({
			loading: props.data.loading,
			error: props.data.error,
			user: props.data.getUser,
		}),
	}),
	graphql(RegisterUser, {
		props: ({ mutate }) => ({
			onRegisterUser: (info) =>
				mutate({
					variables: info,
					optimisticResponse: {
						__typename: 'Mutation',
						registerUser: {
							__typename: 'User',
							userId: info.username,
							firstName: info.firstName,
							phoneNumber: info.phoneNumber,
						},
					},
				}),
		}),
		options: {
			fetchPolicy: 'network-only',
		},
	}),
	graphql(TrackDevice, {
		props: ({ mutate }) => ({
			onTrackDevice: (info) =>
				mutate({
					variables: info,
					optimisticResponse: {
						__typename: 'Mutation',
						trackDevice: {
							__typename: 'Device',
							userId: info.userId,
							deviceId: info.device.deviceId,
							name: info.device.name,
							brand: info.device.brand,
							enabled: info.enabled,
						},
					},
				}),
		}),
	})
)(AuthUserContainer);
