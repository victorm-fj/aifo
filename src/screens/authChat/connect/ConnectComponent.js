// @flow
import React from 'react';
import { withNavigation, NavigationScreenProp } from 'react-navigation';

import { Loading } from '../../../components/chatbot';

type Props = {
	triggerNextStep: Function,
	steps: Object,
	navigation: NavigationScreenProp<*>,
};
class ConnectComponent extends React.Component<Props> {
	componentDidMount() {
		this.props.navigation.navigate('ConnectStack');
		// this.props.navigation.navigate('Invite');
	}
	componentDidUpdate() {
		const { navigation, triggerNextStep } = this.props;
		const connect = navigation.getParam('connect');
		if (connect) {
			triggerNextStep({ trigger: 'successOptions' });
		} else {
			triggerNextStep({ trigger: 'justOpened' });
			setTimeout(() => triggerNextStep({ trigger: 'notifications' }), 0);
		}
	}
	render() {
		return <Loading />;
	}
}

export default withNavigation(ConnectComponent);
