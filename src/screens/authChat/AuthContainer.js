import { Container } from 'unstated';

class AuthContainer extends Container {
	state = {
		cognitoUser: null,
		devices: null,
	};
	addCognitoUser = (cognitoUser) => {
		this.setState({ cognitoUser });
	};
	addDevices = (devices) => {
		this.setState({ devices });
	};
}

export { AuthContainer };
