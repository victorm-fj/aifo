// @flow
import React from 'react';
import { View, Text } from 'react-native';
import { Button } from 'react-native-elements';
import Modal from 'react-native-modal';

import styles from './styles';

type Props = {
	isVisible: boolean,
	onSwipe: Function,
	onAccept: Function,
	onDecline: Function,
	device: null | {
		userId: string,
		deviceId: string,
		name: string,
		brand: string,
		enabled: boolean,
	},
};
const TrackDeviceModal = (props: Props) => (
	<Modal
		isVisible={props.isVisible}
		hideModalContentWhileAnimating
		onSwipe={props.onSwipe}
		swipeDirection="down"
	>
		<View style={styles.modalContainer}>
			<Text>
				You are trying to log in in a new device, please accept or decline the
				action.
			</Text>
			<Text>
				DeviceId:{' '}
				{props.device ? props.device.deviceId : 'deviceId-placeholder'}
			</Text>
			<Text>
				Device name: {props.device ? props.device.name : 'name-placeholder'}
			</Text>
			<Text>
				Device brand: {props.device ? props.device.brand : 'brand-placeholder'}
			</Text>
			<Button title="Accept" onPress={props.onAccept} />
			<Button title="Decline" onPRess={props.onDecline} />
		</View>
	</Modal>
);

export default TrackDeviceModal;
