// @flow
import React from 'react';
import { Dimensions, View } from 'react-native';
import { Button } from 'react-native-elements';
import GridView from 'react-native-super-grid';

import Card from '../../../components/card';

const { width } = Dimensions.get('window');
const oneCards = [{ id: 0, title: 'Card 1' }];
const twoCards = [{ id: 0, title: 'Card 1' }, { id: 1, title: 'Card 2' }];
const threeCards = [
	{ id: 0, title: 'Card 1' },
	{ id: 1, title: 'Card 2' },
	{ id: 2, title: 'Card 3' },
];
const fourCards = [
	{ id: 0, title: 'Card 1' },
	{ id: 1, title: 'Card 2' },
	{ id: 2, title: 'Card 3' },
	{ id: 3, title: 'Card 4' },
];

const getItems = (number: number) => {
	let cards = [];
	switch (number) {
	case 1:
		cards = oneCards;
		break;
	case 2:
		cards = twoCards;
		break;
	case 3:
		cards = threeCards;
		break;
	case 4:
		cards = fourCards;
		break;
	default:
		cards = fourCards;
		break;
	}
	return cards;
};
const getItemDimension = (number: number) => {
	const dividend = Math.round(number / 2);
	return width / dividend - dividend * 30;
};
const getHeight = (numCards: number, itemDimension: number) => {
	let height = itemDimension;
	switch (numCards) {
	case 1:
		height = itemDimension / (numCards * 2) - 40;
		break;
	case 2:
		height = itemDimension - itemDimension / (numCards * 2);
		break;
	case 3:
	case 4:
		height = itemDimension * 2 + 5;
		break;
	default:
		break;
	}
	return height + 20;
};

type Props = {
	triggerNextStep: Function,
	steps: Object,
	items: Array<Object>,
};
class CustomCards extends React.Component<Props> {
	onPressEndHandler = () => {
		this.props.triggerNextStep({ trigger: 'loginSuccess' });
	};
	renderItem = (item: Object) => (
		<Card style={{ flex: 1 }}>
			<Card.Header title={item.title} />
			<Card.Content>
				<Button title="Done" onPress={this.onPressEndHandler} />
			</Card.Content>
		</Card>
	);
	render() {
		const cardValue = this.props.steps.loginSuccessOptions.value;
		const cardNum = Number(cardValue.slice(0, 1));
		const itemDimension = getItemDimension(cardNum);
		console.log('itemDimension', itemDimension);
		return (
			<View
				style={{
					height: getHeight(cardNum, itemDimension),
				}}
			>
				<GridView
					style={{ flex: 1 }}
					itemDimension={itemDimension}
					items={getItems(cardNum)}
					renderItem={this.renderItem}
					spacing={15}
					horizontal={cardValue.slice(1) === 'h'}
				/>
			</View>
		);
	}
}

export default CustomCards;
