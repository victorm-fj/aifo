// @flow
import React from 'react';
import { View } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import isEqual from 'lodash.isequal';

import AuthBot from './AuthBot';
import styles from './styles';
import TrackDeviceModal from './TrackDeviceModal';

type Props = {
	loading: boolean,
	error: Object,
	devices: Array<Object>,
	screenProps: NavigationScreenProp<*>,
	subscribeToNewDevice: Function,
	onTrackDevice: Function,
	addDevices: Function,
	unstatedDevices: any,
};
type State = {
	botKey: number,
	modalVisible: boolean,
	newDevice: null | {
		userId: string,
		deviceId: string,
		phoneNumber: string,
		name: string,
		brand: string,
		enabled: boolean,
	},
};
class AuthChat extends React.Component<Props, State> {
	state = { botKey: 0, modalVisible: false, newDevice: null };
	componentDidMount() {
		const {
			screenProps: { isLoggedIn },
		} = this.props;
		if (isLoggedIn) this.startSubscription();
	}
	componentDidUpdate(prevProps: Props) {
		const {
			devices,
			screenProps: { isLoggedIn },
			addDevices,
			unstatedDevices,
		} = this.props;
		const prevScreenProps = prevProps.screenProps;
		if (prevScreenProps.isLoggedIn !== isLoggedIn) {
			// Start subscription once user has logged in; end it once user has logout
			if (isLoggedIn) this.startSubscription();
			else if (this.subscription) this.subscription();
		}
		const prevDevices = prevProps.devices;
		// When user is to signup/signin there is no devices prop (equals to undefined)
		if (!prevDevices || prevDevices.length === 0) return;
		// Devices prop is defined and user just have one tracked device (used when signing up)
		if (devices && devices.length === 1 && !unstatedDevices) {
			addDevices(devices); // add devices to unstated state
		}
		// Devices has been updates - a new tracked device has beend added but is not enabled
		if (!isEqual(prevDevices, devices)) {
			console.log('A new device has been added');
			addDevices(devices); // update devices to unstated state
			const prevIds = prevDevices.map((prevDevice) => prevDevice.deviceId);
			const currentIds = devices.map((currentDevice) => currentDevice.deviceId);
			const newDeviceId = currentIds.filter((id) => !prevIds.includes(id))[0];
			const newDevice = devices.filter((device) => device.deviceId === newDeviceId)[0];
			if (newDevice && !newDevice.enabled) {
				this.setState({ modalVisible: true, newDevice });
			}
		}
	}
	componentWillUnmount() {
		if (this.subscription) this.subscription();
	}
	onAcceptHandler = () => {
		// Grant permission for the most recent tracked device to sign user in
		const { newDevice } = this.state;
		// If device is not enabled, enable it
		if (newDevice && !newDevice.enabled) {
			const {
				userId, deviceId, phoneNumber, name, brand
			} = newDevice;
			try {
				this.props.onTrackDevice({
					userId,
					device: {
						deviceId, phoneNumber, name, brand
					},
					enabled: true,
				});
			} catch (error) {
				console.log('onTrackDevice error', error);
			}
		}
		this.setState({ modalVisible: false, newDevice: null });
	};
	onDeclineHandler = () => {
		this.setState({ modalVisible: false, newDevice: null });
	};
	startSubscription() {
		console.log('Start subscription');
		this.subscription = this.props.subscribeToNewDevice();
	}
	subscription: any;
	render() {
		const { botKey, modalVisible, newDevice } = this.state;
		return (
			<View style={styles.botContainer}>
				<AuthBot botKey={botKey} />
				<TrackDeviceModal
					isVisible={modalVisible}
					onSwipe={this.onDeclineHandler}
					onAccept={this.onAcceptHandler}
					onDecline={this.onDeclineHandler}
					device={newDevice}
				/>
			</View>
		);
	}
}

export default AuthChat;
