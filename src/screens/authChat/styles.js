import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	authChatContainer: {
		flex: 1,
		alignItems: 'center',
		paddingLeft: 15,
		backgroundColor: '#f3f5f8',
	},
	botContainer: {
		flex: 1,
	},
	modalContainer: {
		flex: 1,
		backgroundColor: '#fff',
		margin: 20,
		padding: 20,
		justifyContent: 'space-around',
		alignItems: 'center',
	},
});
