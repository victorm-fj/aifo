// @flow
import React from 'react';
import DeviceInfo from 'react-native-device-info';
import CarrierInfo from 'react-native-carrier-info';
import { parseNumber, formatNumber } from 'libphonenumber-js';
import { isValidNumber } from 'libphonenumber-js/custom';

import { Loading } from '../../../components/chatbot';

const metadata = require('./metadata.mobile.json');

type Props = {
	triggerNextStep: Function,
	steps: Object,
	id: string,
};
class PhoneNumber extends React.Component<Props> {
	async componentDidMount() {
		const { triggerNextStep, steps, id } = this.props;
		let deviceCountry = '';
		// If user has selected a country from prompts use that value
		if (steps.phoneCountryOptions && steps.phoneCountryOptions.value) {
			deviceCountry = steps.phoneCountryOptions.value;
		} else {
			// Get deviceCountry from device's locale
			deviceCountry = DeviceInfo.getDeviceCountry(); // e.g. "US"
			try {
				// Try to get deviceCountry from carrier
				const carrierCountry = await CarrierInfo.isoCountryCode();
				if (carrierCountry) {
					deviceCountry = carrierCountry.toUpperCase();
				}
			} catch (error) {
				console.log('Error getting carrier country', error);
			}
		}
		// Get phoneNumber from the right step
		const phoneNumber = steps[`phoneNumber${id}`].value.trim();
		// Parse the phoneNumber with the deviceCountry
		const parsedObject = parseNumber(phoneNumber, deviceCountry);
		if (parsedObject.phone && isValidNumber(parsedObject, metadata)) {
			// Phone number is a valid phone number, then continue to signup/signin
			const formattedNumber = formatNumber(parsedObject, 'E.164');
			triggerNextStep({
				value: formattedNumber, // update value to international 'E.164' format
				trigger: 'authUser',
				self: true,
			});
		} else {
			triggerNextStep({
				value: undefined,
				trigger: 'invalidPhone1',
				self: true, // reset value to undefined
			});
			setTimeout(() => triggerNextStep({ trigger: 'phoneNumber2' }), 0);
		}
	}
	render() {
		return <Loading />;
	}
}

export default PhoneNumber;
