// @flow
import React from 'react';

import ChatBot from '../../components/chatbot';
import steps from './steps';

type Props = { botKey: number };
const AuthBot = (props: Props) => (
	<ChatBot
		key={props.botKey}
		// cache
		// cacheName="@authBot"
		style={{ backgroundColor: 'transparent' }}
		steps={steps}
		hideBotAvatar
		hideUserAvatar
		displayChat={[
			'phoneNumber2',
			'confirmCode1',
			'confirmCode2',
			'loginSuccess',
		]}
		handleChat={{
			phoneNumber2: { trigger: 'selectCountryOption' },
			confirmCode1: { trigger: 'changePhoneOptions' },
			confirmCode2: { trigger: 'invalidCodeOptions' },
			loginSuccess: { trigger: 'loginSuccessOptions' },
		}}
		inputMetadata={{
			name: { keyboardType: 'default', placeholder: 'Type your name' },
			phoneNumber1: {
				keyboardType: 'numeric',
				placeholder: 'Enter your phone number',
			},
			phoneNumber2: {
				keyboardType: 'numeric',
				placeholder: 'Enter your phone number',
			},
			confirmCode1: {
				keyboardType: 'numeric',
				placeholder: 'Enter the code',
			},
			confirmCode2: {
				keyboardType: 'numeric',
				placeholder: 'Enter the code',
			},
			loginSuccess: {
				keyboardType: 'default',
				placeholder: 'Type your question',
			},
		}}
		showOptions={{
			phoneCustom2: {
				stepCheckPoint: '3',
				allowedAttempts: 3,
				invalidStepId: 'invalidPhone1',
				secondStepCheckPointId: 'phoneCountryOptions',
			},
			codeCustom2: {
				stepCheckPoint: '4',
				allowedAttempts: 3,
				invalidStepId: 'invalidCode1',
			},
		}}
		resetConditions={{
			trigger: 'confirmCode',
			checkStepId: 'authUser',
			conditionValue: 'registeredUser',
		}}
	/>
);

export default AuthBot;
