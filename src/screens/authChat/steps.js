import React from 'react';
import { View } from 'react-native';

import PhoneNumber from './phone/PhoneNumber';
import CustomCards from './customCards/CustomCards';
import AuthUserContainer from './authUser/AuthUserContainer';
import ConfirmCodeContainer from './confirmCode/ConfirmCodeContainer';
import ConnectComponent from './connect/ConnectComponent';

export default [
	{
		id: '0',
		message:
			'I\'m AIFO, the app that\'s going to help you optimize your business growth without forgetting your personal needs.',
		// trigger: '1',
		trigger: 'savingsAccount',
	},
	{
		id: '1',
		message: 'What\'s your name?',
		trigger: 'name',
	},
	{
		id: 'name',
		user: true,
		trigger: '2',
	},
	{
		id: '2',
		message: ({ previousValue }) =>
			`Hi ${previousValue.trim()}. Nice to meet you [emojiName="wave"]`,
		// trigger: '3',
		trigger: '5', // Remove when done testing plaid conversation flow and uncomment the line above
		// ///// SKIP SIGN UP/IN - JUMPT TO WELCOME MESSAGE
	},
	{
		id: '3',
		message:
			'Can I have your mobile number? I know we just met but I need to check you\'re a real person before I get you set up. [emojiName="blush"]',
		trigger: 'yesNo',
	},
	{
		id: 'yesNo',
		options: [
			{
				value: 'yes',
				label: 'Yes, sure',
				highlighted: true,
				trigger: 'phoneNumber1',
			},
			{ value: 'no', label: 'Nope, not yet', trigger: 'end' },
		],
	},
	{
		id: 'phoneNumber1',
		user: true,
		trigger: 'phoneCustom1',
	},
	{
		id: 'phoneCustom1',
		component: <PhoneNumber id="1" />,
		waitAction: true, // handling async code in lifecycle method
		replace: true,
		asMessage: true,
	},
	{
		id: 'invalidPhone1',
		message: 'I don\'t think that number\'s correct. Can you please try again?',
		trigger: 'phoneNumber2',
	},
	{
		id: 'phoneNumber2',
		user: true,
		trigger: 'phoneCustom2',
	},
	{
		id: 'phoneCustom2',
		component: <PhoneNumber id="2" />,
		waitAction: true, // handling async code in lifecycle method
		replace: true,
		asMessage: true,
	},
	{
		id: 'selectCountryOption',
		options: [
			{
				value: 'selectCountry',
				label: 'Select country code',
				trigger: 'phoneCountryOptions',
			},
		],
	},
	{
		id: 'phoneCountryOptions',
		options: [
			{
				value: 'GB',
				label: 'United Kingdom',
				trigger: 'phoneNumber1',
				highlighted: true,
			},
			{
				value: 'US',
				label: 'United States',
				trigger: 'phoneNumber1',
			},
			{
				value: 'MX',
				label: 'Mexico',
				trigger: 'phoneNumber1',
			},
		],
	},
	{
		id: 'authUser',
		// component: <AuthUser />,
		component: <AuthUserContainer />,
		waitAction: true, // handling async code in lifecycle method
		replace: true,
		asMessage: true,
	},
	{
		id: 'trySignInAgain',
		message:
			'This device is not authorize to let you login, please enable this device from your currently tracked device. Then try again',
		trigger: 'phoneNumber1',
	},
	{
		id: '4',
		message:
			'I\'ve sent you a 6-digit code. Enter it below and we\'re good to go.',
		trigger: 'confirmCode1',
	},
	{
		id: 'confirmCode1',
		user: true,
		trigger: 'codeCustom1',
	},
	{
		id: 'codeCustom1',
		component: <ConfirmCodeContainer id="1" />,
		waitAction: true,
		replace: true,
		asMessage: true,
	},
	{
		id: 'invalidCode1',
		message: 'Nope. That\'s not the right code. Can you try again?',
		trigger: 'confirmCode2',
	},
	{
		id: 'confirmCode2',
		user: true,
		trigger: 'codeCustom2',
	},
	{
		id: 'codeCustom2',
		component: <ConfirmCodeContainer id="2" />,
		waitAction: true,
		replace: true,
		asMessage: true,
	},
	{
		id: 'changePhoneOptions',
		options: [
			{
				value: 'changePhone',
				label: 'Change my number',
				trigger: 'updatePhone',
				highlighted: true,
			},
		],
	},
	{
		id: 'updatePhone',
		message: 'Please enter a new number',
		trigger: 'phoneNumber1',
	},
	{
		id: 'invalidCodeOptions',
		options: [
			{
				value: 'get a new code',
				label: 'Get a new code',
				trigger: 'authUser',
				highlighted: true,
			},
			{
				value: 'change my number',
				label: 'Change my number',
				trigger: 'updatePhone',
			},
		],
	},
	{
		id: '5',
		// steps is an object with all steps in the form '{ stepName: { id, message, value } }'
		message: ({ steps }) => `Welcome back ${steps.name.value}!`,
		// trigger: 'loginSuccess',
		trigger: 'confirmIdentity', // Remove when done testing plaid conversation flow and uncomment the line above
		// ////// JUMPT TO TESTING PLAID LINK INTEGRATION
	},
	{
		id: 'loginSuccess',
		user: true,
		trigger: 'end',
	},
	{
		id: 'loginSuccessOptions',
		options: [
			{
				value: '2v',
				label: '2 Cards',
				highlighted: true,
				trigger: 'customLoading',
			},
			{ value: '4v', label: '4 Cards', trigger: 'customLoading' },
			{ value: '1v', label: 'Card', trigger: 'customLoading' },
			// { value: '3h', label: 'Horizontal Cards', trigger: 'customLoading' },
			{ value: 'savings1', label: 'Savings', trigger: 'end' },
			{ value: 'savings2', label: 'Savings', trigger: 'end' },
			{ value: 'spendings1', label: 'Spendings', trigger: 'end' },
			{ value: 'spendings2', label: 'Spendings', trigger: 'end' },
		],
	},
	{
		id: 'customLoading',
		component: <View />,
		replace: true,
		asMessage: true,
		trigger: 'customCards',
	},
	{
		id: 'customCards',
		component: <CustomCards />,
		waitAction: true,
		replace: true,
		delay: 0,
	},
	// //// Start Plaid flow
	{
		id: 'confirmIdentity',
		options: [
			{
				value: 'confirmed',
				label: 'Yep, that\'s me',
				trigger: 'getStarted',
				highlighted: true,
			},
		],
	},
	{
		id: 'getStarted',
		message: 'Great!, Let\'s get started. [emojiName="+1"]',
		trigger: 'bot',
	},
	{
		id: 'bot',
		message: 'I\'m only a bot, but I\'m pretty powerful.',
		trigger: 'continue',
	},
	{
		id: 'continue',
		options: [
			{
				value: 'continue',
				label: 'Ok, sounds good',
				trigger: 'explain',
				highlighted: true,
			},
		],
	},
	{
		id: 'explain',
		message:
			'You\'re probably wondering how this automatic savings thing works. I\'ll explain.',
		trigger: 'goOn',
	},
	{
		id: 'goOn',
		options: [
			{
				value: 'go',
				label: 'Go on then',
				trigger: 'explain2',
				highlighted: true,
			},
		],
	},
	{
		id: 'explain2',
		message:
			'Every few days, I calculate a small amount you can afford to save, without having an impact on your day-to-day life. [emojiName="sunglasses"]',
		trigger: 'explain3',
	},
	{
		id: 'explain3',
		message:
			'I then move the money into your AIFO savings account for you [emojiName="innocent"] you don\'t need to do anything',
		trigger: 'automatically',
	},
	{
		id: 'automatically',
		options: [
			{
				value: 'automatic',
				label: 'What? Automatically?',
				trigger: 'explain4',
				highlighted: true,
			},
		],
	},
	{
		id: 'explain4',
		message:
			'Yup. Fully automatically, every few days, from your current account into your AIFO savings account.',
		trigger: 'explain5',
	},
	{
		id: 'explain5',
		message:
			'You\'ll save small amounts but over time it could add up to quite a nice stash. [emojiName="money_with_wings"]',
		trigger: 'savingsAccount',
	},
	{
		id: 'savingsAccount',
		options: [
			{
				value: 'surprise',
				label: 'Wow, That sh*t cray',
				trigger: 'dontConnect',
			},
			{
				value: 'create',
				label: 'Sign me up',
				trigger: 'doConnect',
				highlighted: true,
			},
		],
	},
	{
		id: 'doConnect',
		component: <ConnectComponent />,
		waitAction: true,
		asMessage: true,
		replace: true,
	},
	{
		id: 'justOpened',
		message:
			'Great job! You just opened your AIFO savings account! Not bad compared to queueing in a bank for hours, eh? [emojiName="wink"]',
		trigger: 'notifications',
	},
	{
		id: 'notifications',
		message:
			'Would you like to receive notifications when a team member answers you?',
		trigger: 'activateNotifications',
	},
	{
		id: 'activateNotifications',
		options: [
			{
				value: 'activated',
				label: 'Yes',
				trigger: 'askToConnect',
				highlighted: true,
			},
			{ value: 'deactivated', label: 'No', trigger: 'askToConnect' },
		],
	},
	{
		id: 'askToConnect',
		message: 'To start saving you need to connect to your current account.',
		trigger: 'connect',
	},
	{
		id: 'connect',
		options: [
			{ value: 'no', label: 'Do it later', trigger: 'dontConnect' },
			{
				value: 'yes',
				label: 'Connect',
				trigger: 'dontConnect',
				highlighted: true,
			},
		],
	},
	{
		id: 'dontConnect',
		message:
			'Fair play! [emojiName="v"] Explore the commands below and connect your current account when you\'re ready.',
		trigger: 'successOptions',
	},
	{
		id: 'successOptions',
		options: [
			{ value: 'savings', label: 'Savings', trigger: 'end' },
			{ value: 'spending', label: 'Spending', trigger: 'end' },
			{ value: 'save', label: 'Save', trigger: 'end' },
			{ value: 'withdraw', label: 'Widthdraw', trigger: 'end' },
			{
				value: 'connect',
				label: 'Connect',
				trigger: 'end',
				highlighted: true,
			},
			{ value: 'savingLevel', label: 'Saving level', trigger: 'end' },
			{ value: 'liveChat', label: 'Live chat', trigger: 'end' },
			{ value: 'faq', label: 'FAQ', trigger: 'end' },
		],
	},
	// //// End Plaid flow
	{
		id: 'end',
		message: 'End of conversation',
		end: true,
	},
];
