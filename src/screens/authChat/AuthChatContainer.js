// @flow
import React from 'react';
import { compose, graphql } from 'react-apollo';
import { Provider, Subscribe } from 'unstated';

import { GetDevices, TrackDevice, NewDeviceSubscription } from '../../graphQL';
import { AuthContainer } from './AuthContainer';
import AuthChat from './AuthChat';

const AuthChatContainer = (props: Object) => (
	<Provider>
		<Subscribe to={[AuthContainer]}>
			{(authStore) => (
				<AuthChat
					addDevices={authStore.addDevices}
					unstatedDevices={authStore.state.devices}
					{...props}
				/>
			)}
		</Subscribe>
	</Provider>
);

export default compose(
	graphql(GetDevices, {
		skip: (ownProps) => !ownProps.screenProps.isLoggedIn,
		options: (ownProps) => ({
			fetchPolicy: 'network-only',
			variables: { userId: ownProps.screenProps.username },
		}),
		props: (props) => ({
			loading: props.data.loading,
			error: props.data.error,
			devices: props.data.getDevices ? props.data.getDevices.devices : [],
			subscribeToNewDevice: () =>
				props.data.subscribeToMore({
					document: NewDeviceSubscription,
					variables: {
						userId: props.ownProps.screenProps.username,
					},
					updateQuery: (
						prev,
						{
							subscriptionData: {
								data: { subscribeToNewDevice },
							},
						}
					) => ({
						...prev,
						getDevices: {
							__typename: 'Query',
							devices: [
								...prev.getDevices.devices.filter((device) => device.deviceId !== subscribeToNewDevice.deviceId),
								subscribeToNewDevice,
							],
						},
					}),
				}),
		}),
	}),
	graphql(TrackDevice, {
		props: ({ mutate }) => ({
			onTrackDevice: (info) =>
				mutate({
					variables: info,
					optimisticResponse: {
						__typename: 'Mutation',
						trackDevice: {
							__typename: 'Device',
							userId: info.userId,
							deviceId: info.device.deviceId,
							name: info.device.name,
							brand: info.device.brand,
							enabled: info.enabled,
						},
					},
				}),
		}),
	})
)(AuthChatContainer);
