import { StyleSheet, Dimensions } from 'react-native';

const { width } = Dimensions.get('window');
const inputWidth = width - 40;

export default StyleSheet.create({
	screenContainer: {
		flex: 1,
		alignItems: 'center',
		padding: 16,
		paddingBottom: 20,
		backgroundColor: '#f3f5f8',
	},
	scrollContainer: {
		flexGrow: 1,
		alignItems: 'center',
	},
	rowContainer: {
		flexDirection: 'row',
		alignItems: 'center',
		paddingVertical: 16,
	},
	titleStyle: {
		marginTop: 20,
		fontFamily: 'Gotham-Book',
		fontSize: 20,
		fontWeight: 'bold',
	},
	formContainer: {
		flex: 1,
		justifyContent: 'space-around',
		alignItems: 'center',
		paddingVertical: 20,
	},
	inputContainer: {
		width: inputWidth,
		marginBottom: 8,
	},
	inputStyle: {
		fontSize: 17,
		fontFamily: 'Gotham-Book',
		marginLeft: 0,
	},
	inputContainerStyle: {
		borderColor: '#3b7cff',
	},
	formButtonContainer: {
		marginBottom: 20,
	},
	checkboxContainer: {
		backgroundColor: 'transparent',
		borderWidth: 0,
	},
	checkboxTextStyle: {
		fontFamily: 'Gotham-Book',
		fontSize: 15,
		marginRight: 0,
		paddingRight: 0,
	},
	footnoteTextStyle: {
		fontSize: 12,
		fontFamily: 'Gotham-Book',
		marginBottom: 5,
		width: inputWidth,
		textAlign: 'center',
	},
	subheadingText: {
		fontSize: 18,
		fontFamily: 'Gotham-Book',
	},
	paragraphText: {
		fontSize: 14,
		fontFamily: 'Gotham-Book',
	},
	textHighlighted: {
		color: '#3b7cff',
	},
});
