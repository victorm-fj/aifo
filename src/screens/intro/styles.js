import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	introContainer: {
		flex: 1,
		alignItems: 'center',
		paddingVertical: 20,
		backgroundColor: '#f9fafc',
	},
	actionsContainer: {
		justifyContent: 'center',
		alignItems: 'center',
	},
	buttonStyle: {
		paddingVertical: 3,
		paddingHorizontal: 67,
		backgroundColor: '#3b7cff',
		borderRadius: 30,
		// minWidth: 230,
	},
	buttonTitleStyle: {
		fontFamily: 'Gotham-Book',
		fontSize: 16,
		color: '#ffffff',
		letterSpacing: 1,
	},
	buttonContainerStyle: {
		marginBottom: 20,
	},
	footnoteTextStyle: {
		fontFamily: 'Gotham-Book',
		fontSize: 12,
		color: '#8392a7',
	},
	footnoteActionTextStyle: {
		color: '#3b7cff',
	},
});
