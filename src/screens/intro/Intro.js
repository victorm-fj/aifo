// @flow
import * as React from 'react';
import { View, Text } from 'react-native';
import { Button } from 'react-native-elements';
import { NavigationScreenProp } from 'react-navigation';

import Slider from '../../components/slider/Slider';
import slide1 from '../../../assets/images/slide1.png';
import slide2 from '../../../assets/images/slide2.png';
import slide3 from '../../../assets/images/slide3.png';
import styles from './styles';

const slides = [
	{ id: 0, image: slide1, text: 'Save without feeling it copy line two' },
	{ id: 1, image: slide2, text: 'Save without feeling it copy line two' },
	{ id: 2, image: slide3, text: 'Save without feeling it copy line two' },
];

type Props = {
	navigation: NavigationScreenProp<*>,
	screenProps: NavigationScreenProp<*>,
};
type State = { activeIndex: number };
class Intro extends React.Component<Props, State> {
	componentDidUpdate(prevProps: Object) {
		const { isLoggedIn } = this.props.screenProps;
		if (prevProps.screenProps.isLoggedIn !== isLoggedIn && isLoggedIn) {
			this.props.navigation.replace('AuthChat');
		}
	}
	render() {
		return (
			<View style={styles.introContainer}>
				<Slider slides={slides} />
				<View style={styles.actionsContainer}>
					<Button
						title="Get started"
						onPress={() => this.props.navigation.replace('AuthChat')}
						titleStyle={styles.buttonTitleStyle}
						buttonStyle={styles.buttonStyle}
						containerStyle={styles.buttonContainerStyle}
					/>
					<Text style={styles.footnoteTextStyle}>
						By continuing you agree to our{' '}
						<Text style={styles.footnoteActionTextStyle}>Terms of use</Text>.
					</Text>
				</View>
			</View>
		);
	}
}

export default Intro;
