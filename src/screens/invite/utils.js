import { Alert } from 'react-native';
import Contacts from 'react-native-contacts';

export const letters = [
	'a',
	'b',
	'c',
	'd',
	'e',
	'f',
	'g',
	'h',
	'i',
	'j',
	'k',
	'l',
	'm',
	'n',
	'o',
	'p',
	'q',
	'r',
	's',
	't',
	'u',
	'v',
	'w',
	'x',
	'y',
	'z',
	'#',
];

export const getContactsList = () =>
	new Promise((resolve, reject) => {
		Contacts.getAll((error, contacts) => {
			if (error && error === 'denied') {
				console.log('getContactsList error -', error);
				Alert.alert(
					'Can\'t access your contacts list',
					'Please go to Settings > Privacy > Contacts, to allow \'AIFO\' to access your Contacts',
					[{ text: 'OK', onPress: () => {} }]
				);
				reject(error);
				return;
			}
			resolve(contacts);
		});
	});
