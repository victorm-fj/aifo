// @flow
import React from 'react';
import { View, Text, Platform, Alert } from 'react-native';
import { SearchBar, CheckBox, Divider } from 'react-native-elements';
import { NavigationScreenProp } from 'react-navigation';
import isEqual from 'lodash.isequal';

import Loader from '../../components/Loader';
import PartnerTracker from './PartnerTracker';
import requirePermissions from '../../components/hocs/requirePermissions';
import SearchableSectionList from '../../components/SearchableSectionList';
import commonStyles from '../commonStyles';
import styles from './styles';
import { letters, getContactsList } from './utils';

type Props = { navigation: NavigationScreenProp<*> };
type State = {
	loading: boolean,
	contacts: Array<Object>,
	invitee: string,
	searchTerm: string,
	sent: boolean,
};
class Contacts extends React.Component<Props, State> {
	state = {
		loading: true,
		invitee: '',
		contacts: [],
		searchTerm: '',
		sent: false,
	};
	componentDidMount() {
		this.getContactsList();
	}
	componentDidUpdate(prevProps) {
		const { state } = this.props.navigation;
		if (state.params && !isEqual(prevProps.navigation.state, state)) {
			if (state.params.sent === this.state.invitee) {
				this.setState({ sent: true });
			}
		}
	}
	onPressCheckBox = (recordID) => {
		this.props.navigation.setParams({ recordID });
		this.setState((prevState) => {
			let invitee = recordID;
			if (prevState.invitee === recordID) {
				invitee = '';
			}
			return { invitee };
		});
	};
	onSearch = (searchTerm) => {
		this.setState({ searchTerm: searchTerm.trim() });
	};
	onCancelSearch = () => {
		this.setState({ searchTerm: '' });
		// this.searchBar.clear();
	};
	async getContactsList() {
		let contactsList = [];
		try {
			contactsList = await getContactsList();
		} catch (error) {
			console.log('getting contacts error', error);
			this.displayErrorAlert();
		} finally {
			if (contactsList.length > 0) {
				const contacts = contactsList.map((contact) => ({
					...contact,
					searchStr: `${contact.givenName} ${contact.familyName}`,
				}));
				this.setState({ contacts, loading: false });
			}
		}
	}
	getSections() {
		const { contacts } = this.state;
		const sectionsObj = {};
		letters.forEach((letter) => {
			const title = letter.toUpperCase();
			sectionsObj[title] = { title, data: [] };
		});
		contacts.forEach((contact) => {
			const firstLetter = contact.givenName.slice(0, 1).toUpperCase();
			if (firstLetter.match(/[a-z]/i)) {
				sectionsObj[firstLetter].data.push(contact);
			} else {
				sectionsObj['#'].data.push(contact);
			}
		});
		return Object.values(sectionsObj).filter((section) => section.data.length > 0);
	}
	displayErrorAlert = () => {
		Alert.alert('Error', 'Something went wrong, can not get contacts list', [
			{ text: 'OK', onPress: () => {} },
		]);
	};
	renderSectionHeader = ({ section: { title } }) => (
		<View style={styles.sectionHeader}>
			<Text style={styles.headerTitle}>{title}</Text>
		</View>
	);
	renderItem = ({ item }) => {
		const {
			recordID, givenName, middleName, familyName
		} = item;
		const checked = recordID === this.state.invitee;
		const title = `${givenName || ''} ${middleName || ''} ${familyName || ''}`;
		return (
			<CheckBox
				key={recordID}
				title={title}
				checked={checked}
				textStyle={commonStyles.checkboxTextStyle}
				containerStyle={commonStyles.checkboxContainer}
				onPress={() => this.onPressCheckBox(recordID)}
				checkedColor="#3b7cff"
				checkedIcon="ios-checkbox-outline"
				uncheckedIcon="ios-checkbox"
				iconType="ionicon"
			/>
		);
	};
	render() {
		const { contacts, invitee } = this.state;
		const inviteePartnerName = invitee
			? contacts.filter((contact) => contact.recordID === invitee)[0].givenName
			: 'Dia Danciu';
		return (
			<View
				style={[commonStyles.screenContainer, { padding: 0, paddingBottom: 0 }]}
			>
				<SearchBar
					ref={(searchBar) => (this.searchBar = searchBar)}
					platform={Platform.OS}
					placeholder="Search"
					onChangeText={this.onSearch}
					onCancel={this.onCancelSearch}
					onClear={this.onCancelSearch}
					containerStyle={styles.containerSearchBar}
				/>
				<View
					style={{
						padding: 8,
						backgroundColor: '#eeeeee',
						width: '100%',
					}}
				>
					<Text
						style={{
							fontFamily: 'Gotham-Book',
							fontSize: 16,
							color: '#3b4859',
						}}
					>
						Tap your fiends to send them a nudge
					</Text>
				</View>
				<PartnerTracker />
				{this.state.sent && <PartnerTracker partnerName={inviteePartnerName} />}
				{this.state.loading ? (
					<Loader />
				) : (
					<SearchableSectionList
						style={styles.container}
						searchProperty="searchStr"
						searchTerm={this.state.searchTerm}
						renderItem={this.renderItem}
						renderSectionHeader={this.renderSectionHeader}
						keyExtractor={(item, index) => String(index)}
						sections={this.getSections()}
						ItemSeparatorComponent={() => (
							<Divider style={{ backgroundColor: '#eeeeee' }} />
						)}
					/>
				)}
			</View>
		);
	}
}

export default requirePermissions(Contacts, ['contacts']);
