// @flow
import React from 'react';
import { View, Text } from 'react-native';
import { ListItem, Slider } from 'react-native-elements';

type Props = {
	// partnerId: string,
	partnerName?: string,
	// status: number,
};
const PartnerTracker = (props: Props) => (
	<ListItem
		containerStyle={{ width: '100%', paddingBottom: 0 }}
		title={props.partnerName}
		titleStyle={{ fontFamily: 'Gotham-Book', fontSize: 17 }}
		subtitle={
			<View>
				<View
					style={{
						flexDirection: 'row',
						paddingTop: 5,
						justifyContent: 'space-between',
					}}
				>
					<Text
						style={{
							fontFamily: 'Gotham-Book',
							fontSize: 10,
							color: '#bdbdbd',
						}}
					>
						INVITE SENT
					</Text>
					<Text
						style={{
							fontFamily: 'Gotham-Book',
							fontSize: 10,
							color: '#bdbdbd',
						}}
					>
						ACCOUNT CREATED
					</Text>
					<Text
						style={{
							fontFamily: 'Gotham-Book',
							fontSize: 10,
							color: '#bdbdbd',
						}}
					>
						SAVED
					</Text>
				</View>
				<Slider
					value={1}
					// onValueChange={(value) => this.setState({ value })}
					minimumValue={1}
					maximumValue={3}
					disabled
					step={1}
					thumbTouchSize={{ widht: 20, height: 20 }}
					trackStyle={{ height: 8 }}
				/>
			</View>
		}
		bottomDivider
	/>
);
PartnerTracker.defaultProps = {
	partnerName: 'Dia Danciu',
};

export default PartnerTracker;
