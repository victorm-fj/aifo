import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	container: {
		flex: 1,
		width: '100%',
	},
	text: {
		fontSize: 18,
	},
	sectionHeader: {
		height: 32,
		paddingLeft: 10,
		backgroundColor: 'rgb(237,240,245)',
		justifyContent: 'center',
	},
	headerTitle: {
		fontSize: 16,
		fontWeight: 'bold',
	},
	footer: {
		height: 44,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderTopColor: '#d8d8d8',
		backgroundColor: 'rgb(239,239,244)',
		justifyContent: 'center',
	},
	titleStyle: {
		color: 'rgb(0,122,255)',
		fontWeight: 'bold',
	},
	buttonContainer: {
		alignSelf: 'flex-end',
		marginRight: 5,
	},
	containerSearchBar: {
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderBottomColor: '#d8d8d8',
	},
});
