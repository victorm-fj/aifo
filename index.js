import { AppRegistry } from 'react-native';
import { Client } from 'bugsnag-react-native';

import App from './src/App';

const bugsnag = new Client();
// bugsnag.notify(new Error('Test error'));

AppRegistry.registerComponent('AIFO', () => App);
